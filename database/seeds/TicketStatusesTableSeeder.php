<?php

use Illuminate\Database\Seeder;

class TicketStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('ticket_statuses')->insert([
            [
                'name'=>'new'
            ],[
                'name'=>'asked'
            ],[
                'name'=>'answered'
            ],[
                'name'=>'closed'
            ]
        ]);
    }
}
