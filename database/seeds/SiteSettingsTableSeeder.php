<?php

use Illuminate\Database\Seeder;

class SiteSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('site_settings')->insert([
            [
                'name'=>'payment_method',
                'value'=>1,
                'type'=>'integer'
            ],
            [
                'name'=>'item_lock_time',
                'value'=>5*60,
                'type'=>'integer'
            ],
            [
                'name'=>'account_keeper_delay_authorised',
                'value'=>5*60,
                'type'=>'integer'
            ],
            [
                'name'=>'account_keeper_delay_captcha',
                'value'=>5*60,
                'type'=>'integer'
            ],
            [
                'name'=>'anticaptcha_key',
                'value'=>"",
                'type'=>'string'
            ]
        ]);
    }
}
