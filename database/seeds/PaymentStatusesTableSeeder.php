<?php

use Illuminate\Database\Seeder;

class PaymentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('payment_statuses')->insert([
            [
                'name'=>'created'
            ],[
                'name'=>'pending'
            ],[
                'name'=>'complete'
            ],[
                'name'=>'cancelled'
            ],[
                'name'=>'error'
            ],
        ]);
    }
}
