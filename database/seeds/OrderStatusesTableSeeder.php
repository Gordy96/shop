<?php

use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('order_statuses')->insert([
            [
                'name'=>'created'
            ],[
                'name'=>'active'
            ],[
                'name'=>'complete'
            ],[
                'name'=>'cancelled'
            ],[
                'name'=>'error'
            ],
        ]);
    }
}
