<?php

use Illuminate\Database\Seeder;

class AccountStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('account_statuses')->insert([
            [
                'name'=>'created'
            ],[
                'name'=>'active'
            ],[
                'name'=>'inactive'
            ],[
                'name'=>'suspended'
            ],[
                'name'=>'error'
            ],[
                'name'=>'mail_confirmation'
            ],
        ]);
    }
}
