<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('payment_methods')->insert([
            [
                'name'=>'exmo',
                'class'=>\App\Helpers\Payments\Platforms\ExmoApi::class,
                'info'=>json_encode([
                    'payeer'=>[
                        "commission"=>3.0,
                        "range"=>[25,10000]
                    ],
                    'yandex'=>[
                        "commission"=>5.9,
                        "range"=>[25,10000]
                    ]
                ])
            ],[
                'name'=>'livecoin',
                'class'=>\App\Helpers\Payments\Platforms\LiveCoinApi::class,
                'info'=>json_encode([
                    'qiwi'=>[
                        "commission"=>7.0,
                        "range"=>[1000,10000]
                    ]
                ])
            ]
        ]);
    }
}
