<?php

use Illuminate\Database\Seeder;

class MediaTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('media_types')->insert([
            [
                'name'=>'image'
            ],[
                'name'=>'video'
            ]
        ]);
    }
}
