<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('product_categories')->insert([
            [
                'name'=>'game_key'
            ],[
                'name'=>'game_account'
            ],[
                'name'=>'game_money'
            ],[
                'name'=>'gift_card'
            ],
        ]);
    }
}
