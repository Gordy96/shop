<?php

use Illuminate\Database\Seeder;

class ItemStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('item_statuses')->insert([
            [
                'name'=>'available'
            ],[
                'name'=>'reserved'
            ],[
                'name'=>'sold'
            ],
        ]);
    }
}
