<?php

use Illuminate\Database\Seeder;

class ProductAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('product_attributes')->insert([
            [
                'name'=>'year'
            ], [
                'name'=>'genre'
            ]
        ]);
    }
}
