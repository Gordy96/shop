<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AccountSettingsTableSeeder::class);
        $this->call(AccountStatusesTableSeeder::class);
        $this->call(ItemStatusesTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
        $this->call(PaymentStatusesTableSeeder::class);
//        $this->call(ProductAttributesTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);
        $this->call(SiteSettingsTableSeeder::class);
        $this->call(MediaTypesTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(TicketCategoriesTableSeeder::class);
        $this->call(TicketStatusesTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
    }
}
