<?php

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('translations')->insert([
            [
                'locale' => 'en',
                'key' => 'pages.home.search.field',
                'value' => 'e.g. Foo bar',
            ],[
                'locale' => 'en',
                'key' => 'links.faq',
                'value' => 'FAQ',
            ],[
                'locale' => 'en',
                'key' => 'links.admin',
                'value' => 'Admin panel',
            ],[
                'locale' => 'en',
                'key' => 'links.logout',
                'value' => 'Logout',
            ],[
                'locale' => 'en',
                'key' => 'links.support',
                'value' => 'Support',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.livecoin.number',
                'value' => 'Enter your qiwi number',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.proceed',
                'value' => 'Proceed',
            ],[
                'locale' => 'en',
                'key' => 'support.contact',
                'value' => 'Your contacts',
            ],[
                'locale' => 'en',
                'key' => 'support.theme',
                'value' => 'Subject',
            ],[
                'locale' => 'en',
                'key' => 'support.category',
                'value' => 'Category',
            ],[
                'locale' => 'en',
                'key' => 'support.submit',
                'value' => 'Create ticket',
            ],[
                'locale' => 'en',
                'key' => 'support.cancel',
                'value' => 'Cancel',
            ],[
                'locale' => 'en',
                'key' => 'pages.home.category',
                'value' => 'Category',
            ],[
                'locale' => 'en',
                'key' => 'pages.home.popular',
                'value' => 'Most popular',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.order.incomplete',
                'value' => 'Your order isn`t complete yet',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.order.complete',
                'value' => 'Order is complete. Your items listed below:',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.order.main_info',
                'value' => 'Click the "proceed" button to continue payment',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.order.button_info',
                'value' => 'Clicking the button will open new tab for payment',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.order.exceeded',
                'value' => 'Sorry order exceeded time limit. Please wait here a bit it still might succeed (Contact our support if not)',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.download',
                'value' => 'Download',
            ],[
                'locale' => 'en',
                'key' => 'pages.checkout.check',
                'value' => 'Check order',
            ],[
                'locale' => 'en',
                'key' => 'links.home',
                'value' => 'Home',
            ],[
                'locale' => 'en',
                'key' => 'pages.home.filters',
                'value' => 'Filters',
            ],[
                'locale' => 'en',
                'key' => 'pages.home.search.results',
                'value' => 'Search ":q":',
            ],[
                'locale' => 'en',
                'key' => 'pages.support.title',
                'value' => 'Support',
            ],[
                'locale' => 'en',
                'key' => 'support.message',
                'value' => 'Message',
            ],[
                'locale' => 'en',
                'key' => 'pages.support.send',
                'value' => 'Send message',
            ],[
                'locale' => 'en',
                'key' => 'products.categories.game_key',
                'value' => 'Game key',
            ],[
                'locale' => 'en',
                'key' => 'products.categories.game_account',
                'value' => 'Game account',
            ],[
                'locale' => 'en',
                'key' => 'products.categories.game_money',
                'value' => 'Game money',
            ],[
                'locale' => 'en',
                'key' => 'products.categories.gift_card',
                'value' => 'Gift card',
            ],[
                'locale' => 'en',
                'key' => 'products.categories.game_item',
                'value' => 'Game item',
            ],[
                'locale' => 'en',
                'key' => 'products.cart.add',
                'value' => 'Add',
            ],[
                'locale' => 'en',
                'key' => 'cart.title',
                'value' => 'Cart',
            ],[
                'locale' => 'en',
                'key' => 'cart.contact',
                'value' => 'Contact',
            ],[
                'locale' => 'en',
                'key' => 'cart.payment.method',
                'value' => 'Payment method',
            ],[
                'locale' => 'en',
                'key' => 'cart.close',
                'value' => 'Close',
            ],[
                'locale' => 'en',
                'key' => 'cart.checkout',
                'value' => 'Checkout',
            ],[
                'locale' => 'en',
                'key' => 'errors.api.cart.empty',
                'value' => 'Cart is empty',
            ],[
                'locale' => 'en',
                'key' => 'support.warn',
                'value' => 'Save this key to have access to this page <strong>:hash</strong>',
            ],[
                'locale' => 'en',
                'key' => 'products.filters.year',
                'value' => 'Year',
            ],[
                'locale' => 'en',
                'key' => 'pages.login.submit',
                'value' => 'Login',
            ],[
                'locale' => 'en',
                'key' => 'pages.login.title',
                'value' => 'Login',
            ],[
                'locale' => 'en',
                'key' => 'pages.login.email',
                'value' => 'Email',
            ],[
                'locale' => 'en',
                'key' => 'pages.login.password',
                'value' => 'Password',
            ],[
                'locale' => 'en',
                'key' => 'pages.login.remember',
                'value' => 'Remember me',
            ],[
                'locale' => 'en',
                'key' => 'products.cart.added',
                'value' => 'Item added',
            ],[
                'locale' => 'en',
                'key' => 'cart.payment.methods.payeer',
                'value' => 'Payeer',
            ],[
                'locale' => 'en',
                'key' => 'cart.payment.methods.yandex',
                'value' => 'Yandex',
            ],[
                'locale' => 'en',
                'key' => 'cart.payment.methods.qiwi',
                'value' => 'Qiwi',
            ],[
                'locale' => 'en',
                'key' => 'support.categories.other',
                'value' => 'Other',
            ],[
                'locale' => 'en',
                'key' => 'pages.support.support_name',
                'value' => 'Support',
            ],[
                'locale' => 'en',
                'key' => 'pagination.previous',
                'value' => 'Previous',
            ],[
                'locale' => 'en',
                'key' => 'pagination.next',
                'value' => 'Next',
            ]
        ]);
    }
}
