<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')
                ->on('ticket_categories')
                ->onDelete('cascade');

            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')
                ->references('id')
                ->on('ticket_statuses')
                ->onDelete('cascade');

            $table->integer('owner_id')->unsigned()->nullable();
            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('assignee_id')->unsigned()->nullable();
            $table->foreign('assignee_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->string('contact', 64)->index();
            $table->string('key')->index();

            $table->string('theme');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
