import Vue from 'vue'
import Notification from './Notification'

export default function(params) {
    let message;
    if (typeof params === 'string') message = params;

    const defaultParam = { message };
    const propsData = Object.assign(defaultParam, params);

    const NotificationComponent = Vue.extend(Notification);
    return new NotificationComponent({
        el: document.createElement('div'),
        propsData
    })
}
