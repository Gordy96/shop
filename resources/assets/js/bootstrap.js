
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

Number.prototype.round = function(places) {
    return +(Math.round(this + "e+" + places)  + "e-" + places);
};

window.Vue = require('vue');
import Trans from "./trans.js";
const trans = Trans;
trans.lang = window.translations;


import Notification from "./components/notification/index"
Vue.$notify = Notification;
Vue.prototype.$notify = Notification;

Vue.mixin({
    methods: {
        parseQuery : function(q){
            if(q[0] === '?')
                q = q.substr(1);
            let temp = {};
            if(!q.length)
                return temp;
            q.split('&').forEach(function(e){
                let t = e.split('=');
                if(t[0].indexOf('[') > 0){
                    let pname = t[0].substr(0, t[0].indexOf('['));
                    if(!temp.hasOwnProperty(pname))
                        temp[pname] = [];
                    temp[pname].push(t[1])
                } else {
                    temp[t[0]] = t[1].length ? t[1] : null;
                }
            });
            return temp;
        },
        isAssoc: function(obj){
            let a = true;
            let keys = Object.keys(obj);
            for(let i = 0; i < keys.length; i++)
                a = keys[i] !== ""+i;
            return typeof obj === 'object' && a;
        },
        makeQuery: function(params){
            let esc = encodeURIComponent;
            let self = this;
            return Object.keys(params).filter(function(k){
                if(params[k]){
                    if (typeof params[k] === "string") {
                        if (params[k].length > 0)
                            return true;
                    } else {
                        return true;
                    }
                }
            }).map(function(k){
                let r;
                if(typeof params[k] === 'object' && !self.isAssoc(params[k])){
                    r = esc(k)+"[]" + '=' + esc(params[k][0]);
                    for(let i = 1; i < params[k].length; i++)
                        r += "&" + esc(k)+"[]" + '=' + esc(params[k][i]);
                    return r;
                }
                return esc(k) + '=' + esc(params[k])
            }).join('&');
        },
        _tr: function(name, args) {
            return trans._tr(name, args)
        },
        prepareParameterString(data){
            let params = {};
            let temp = {};
            for(let e in data){
                temp[e] = [];
                for(let value in data[e]){
                    if(data[e][value] === true){
                        temp[e].push(value);
                    }
                }
                switch (temp[e].length){
                    case 0:
                        break;
                    case 1:
                        params[e] = temp[e][0];
                        break;
                    default:
                        params[e] = temp[e];
                }
            }
            return params;
        }
    }
});

import Buefy from "buefy";

Vue.use(Buefy,{
    defaultIconPack: 'fa'
});