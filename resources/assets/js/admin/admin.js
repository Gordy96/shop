import Toast from "buefy/src/components/toast/index";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('new-product-form', require('../components/admin/NewProductFormComponent.vue'));
Vue.component('edit-product-form', require('../components/admin/EditProductFormComponent.vue'));
Vue.component('product-select-form', require('../components/admin/ProductSelectForm.vue'));
Vue.component('item-list', require('../components/admin/ItemList.vue'));
Vue.component('paginator', require('../components/PaginatorComponent.vue'));
Vue.component('date-picker', require('../components/admin/DatePickerComponent.vue'));

const app = new Vue({
    el: '#app'
});
