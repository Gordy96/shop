const Trans = {
    lang : {},
    options : {
        default_language : 'en',
        current_language : 'en'
    },
    _tr : function (name, args) {
        let parts = name.split('.');
        let temp = this.lang;

        for(let i = 0; i < parts.length; i++){
            let val = temp[parts[i]];
            if (typeof val == "undefined") {
                return name;
            }
            temp = val;
        }
        if(args)
            Object.keys(args).forEach(function(v){
                temp = temp.replace(':' + v, args[v])
            });
        return temp;
    }
};
export default Trans;