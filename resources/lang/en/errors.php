<?php
return [
    "api"=>[
        "invalid_total"=>[
            "below"=>"Cart total is too small",
            "above"=>"You`ve reached top limit",
            "fatal"=>"Unknown error"
        ],
        "cart"=>[
            "empty"=>"Cart is empty"
        ]
    ]
];