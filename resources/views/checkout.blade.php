@extends('layouts.checkout')

@if($payment->order->status->name === 'complete')
    @php($download = true)
@elseif(in_array($payment->order->status->name, ['created', 'active', 'pending','cancelled']))
    @php($download = false)
@endif

@section('styles')
    @if(!$download)
    <meta http-equiv="refresh" content="60">
    @endif
    @parent
@endsection

@section('body')
    <div class="container">
        @if(!$download)
        <div class="box">
            <section class="section">
                <div class="content" style="max-height: 600px; overflow-y: scroll;">
                    @include('components.instruction')
                </div>
            </section>
        </div>
        @endif
        <div class="has-text-centered">
            <p class="title">
                @if(!$download)
                    {{trans("pages.checkout.order.incomplete")}}
                @else
                    {{trans("pages.checkout.order.complete")}}
                @endif
            </p>
            @if(!$download)
                @if(!in_array($payment->order->status->name, ['cancelled']))
                <p class="subtitle">
                    <span>
                        {{trans("pages.checkout.order.main_info")}}
                    </span>
                </p>
                <p class="subtitle">
                    <span>{{trans("pages.checkout.order.button_info")}}</span>
                </p>
                @else
                <p class="subtitle">
                    <span>{{trans("pages.checkout.order.exceeded")}}</span>
                </p>
                @endif
            @endif
            <div class="">
                @if($download)
                    @if(isset($data))
                        <pre>{!! $data !!}</pre>
                    @else
                    <a class="button is-info is-large is-inverted" href="{{url()->current()}}?action=download">
                            <span class="icon">
                              <i class="fa fa-download"></i>
                            </span>
                        <span>{{trans("pages.checkout.download")}}</span>
                    </a>
                    @endif
                @else
                    <div class="columns checkout">
                        <div class="column is-3 is-offset-3">
                        @if(!in_array($payment->order->status->name, ['cancelled']))
                            {!! $payment->url !!}
                        @else
                            <a class="button is-info is-large is-inverted" disabled>
                                <span class="icon">
                                  <i class="fa fa-credit-card"></i>
                                </span>
                                    <span>{{trans("pages.checkout.proceed")}}</span>
                            </a>
                        @endif
                        </div>
                        <div class="column is-3">
                            <a class="button is-info is-large is-inverted" href="{{url()->current()}}">
                                <span class="icon">
                                  <i class="fa fa-refresh"></i>
                                </span>
                                <span>{{trans("pages.checkout.check")}}</span>
                            </a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection