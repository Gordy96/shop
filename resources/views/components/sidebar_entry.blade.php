<li>
    @php($category = array_shift($source))
    <a href="/{{$category->name}}{{request()->has('q') ? '?q='.request()->get('q') : ''}}">{{trans("products.categories.".$category->name)}}</a>
    @if(count($source))
    <ul>
        @include('components.sidebar_entry', ['source'=>$source])
    </ul>
    @endif
</li>