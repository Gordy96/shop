<div class="card">
    <div class="card-image">
        <figure class="image is-1by1">
            @if(($image = $product->images()->first()))
            <img src="{{asset('storage/'.(file_exists(storage_path('app/public/' . $image->first()->name)) ? $image->first()->name : 'none.png'))}}">
            @else
                <img src="{{asset('storage/none.png')}}">
            @endif
        </figure>
    </div>
    <div class="card-content">
        <div class="content">
            <div class="item-title"><a href="{{$prefix}}/{{$product->slug}}">{{$product->name}}</a></div>
        </div>
        <div class="content-footer has-text-right">
            <a data-filename="https://www.lottiefiles.com/storage/datafiles/imdF9U9Xrs5M9TyStloHZCZbitqk6mIVt7JgxIvo/Scan sensor/scan sensor.json" class="show_qr button is-light" title="Preview on the LottiePreview app">
                <span class="icon is-small">
                    <i class="fa fa-qrcode" aria-hidden="true"></i>
                </span>
            </a>
            <a href="" title="" class="button is-success">
                Add to cart
            </a>
        </div>
    </div>
</div>