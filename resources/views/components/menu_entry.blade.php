<li>
    <a href="{{$link}}" @if(ends_with(url()->current(), $link) )class="is-active"@endif>{{$name}}</a>
    @if(isset($sub_links))
    <ul>
        {{$sub_links}}
    </ul>
    @endif
</li>