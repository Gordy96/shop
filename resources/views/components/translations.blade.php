<script>
@php($groups = \App\Translation::query()
->selectRaw("substr(`key`,1, locate('.', `key`) - 1) as `key`")
->groupBy(DB::raw("substr(`key`,1, locate('.', `key`) - 1)"))
->pluck('key'))
    window.translations = {
@foreach($groups as $group)
@if(!in_array($group, $forbidden))
        {{$group}} : {!! json_encode(array_merge((is_array($tgen = trans($group, [], 'en'))) ? $tgen : [],trans($group))) !!},
@endif
@endforeach
    };
</script>