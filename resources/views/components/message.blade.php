<article class="message {{isset($type) ? "is-{$type}" : ""}}">
    <div class="message-body">
        <article class="media">
            <figure class="media-left">
                <p class="image is-64x64">
                    <img src="{{$img}}">
                </p>
            </figure>
            <div class="media-content">
                <div class="content">
                    <p>
                        <strong>{{$name}}</strong>
                        <br>
                        {{$text}}
                    </p>
                </div>
            </div>
            <div class="media-right">
                <small>{{$date}}</small>
            </div>
        </article>
    </div>
</article>