<section class="section">
    @if(isset($hash))
    <article class="message is-danger">
        <div class="message-header">
            <p>{{trans("pages.support.title")}}</p>
        </div>
        <div class="message-body">
            <div class="media">
                <div class="media-left">
                    <span class="icon is-large">
                        <i class="fa fa-2x fa-exclamation-circle"></i>
                    </span>
                </div>
                <div class="media-content">
                    {!! trans("support.warn", ["hash"=>$hash]) !!}
                </div>
            </div>
        </div>

    </article>
    @endif
    <div class="box">
        @foreach($ticket->messages as $message)
            @if($ticket->assignee && ($message->user && $ticket->assignee->id === $message->user->id))
                @php($icon = file_get_contents(resource_path('views/support/support_icon.blade.php')))
                @include('components.message', ['type'=>'info', 'img'=>$icon, 'name'=>trans('pages.support.support_name'), 'text'=>$message->text, 'date'=>$message->created_at])
            @else
                @include('components.message', ['img'=>'', 'name'=>$ticket->contact, 'text'=>$message->text, 'date'=>$message->created_at])
            @endif
        @endforeach
    </div>
    <div class="box">
        <form method="post">
            <article class="media">
                <figure class="media-left">
                    <p class="image is-64x64">
                        <img src="">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="field">
                        <p class="control">
                            <textarea class="textarea" name="text" placeholder="{{trans("support.message")}}"></textarea>
                        </p>
                    </div>
                    <nav class="level">
                        <div class="level-left">
                            <div class="level-item">
                                <button type="submit" class="button is-info">{{trans("pages.support.send")}}</button>
                            </div>
                        </div>
                    </nav>
                </div>
            </article>
            {{csrf_field()}}
        </form>
    </div>
</section>