@extends('layouts.main')
@section('body')
    <section class="section">
        <div class="box">
            <form method="post" action="{{url('/support')}}">
                <div class="field">
                    <label class="label">{{trans("support.contact")}}</label>
                    <div class="control">
                        <input class="input" name="contact" type="text" placeholder="{{trans("support.contact")}}">
                    </div>
                </div>
                <div class="field">
                    <label class="label">{{trans("support.theme")}}</label>
                    <div class="control">
                        <input class="input" name="theme" type="text" placeholder="{{trans("support.theme")}}">
                    </div>
                </div>
                <div class="field">
                    <label class="label">{{trans("support.category")}}</label>
                    <div class="control">
                        <div class="select">
                            <select name="category">
                                @foreach(\App\TicketCategory::all() as $category)
                                <option value="{{$category->id}}">{{trans("support.categories.".$category->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label class="label">{{trans("support.message")}}</label>
                    <div class="control">
                        <textarea name="first_message" class="textarea" placeholder="{{trans("support.message")}}"></textarea>
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-link" type="submit">{{trans("support.submit")}}</button>
                    </div>
                    <div class="control">
                        <a class="button is-text" href="/">{{trans("support.cancel")}}</a>
                    </div>
                </div>
                {{csrf_field()}}
            </form>
        </div>
    </section>
@endsection