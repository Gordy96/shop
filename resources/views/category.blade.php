@extends('layouts.main')
@section('body')
    <section class="section">
        <div class="columns is-multiline">
            <div class="column is-2 filter">
                <aside class="menu">
                    <p class="menu-label">
                        {{trans("pages.home.category")}}
                    </p>
                    <ul class="menu-list">
                        <li>
                            <a href="/">
                                {{trans("links.home")}}
                            </a>
                            <ul>
                                @php($names = $category->getParentChain())
                                @include('components.sidebar_entry', ['source'=>$names])
                            </ul>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column products is-10">
                <h3 class="title is-3">{{trans("products.categories.".$category->name)}}</h3>
                @foreach($category->sub_categories as $sub)
                    <a href="/{{$sub->name}}">{{trans("products.categories.".$sub->name)}}</a>
                @endforeach
            </div>
        </div>
    </section>
@endsection