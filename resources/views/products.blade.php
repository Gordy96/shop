@extends('layouts.main')
@section('body')
    <section class="section">
        <div class="columns is-multiline">
            <div class="column is-2 filter">
                <aside class="menu">
                    <p class="menu-label">
                        Category
                    </p>
                    <ul class="menu-list">
                        <li>
                            <a href="/">{{trans("links.home")}}</a>
                            <ul>
                                @if(isset($category))
                                    @php($names = $category->getParentChain())
                                    @include('components.sidebar_entry', ['source'=>$names])
                                @elseif(isset($search))
                                    @foreach($search as $category)
                                        @php($names = $category->getParentChain())
                                        @include('components.sidebar_entry', ['source'=>$names])
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                    </ul>
                    <p class="menu-label">
                        {{trans('pages.home.filters')}}
                    </p>
                    <ul class="menu-list">
                        <filter-panel></filter-panel>
                    </ul>
                </aside>
            </div>
            <div class="column products is-10">
                @if(request()->has('q'))
                    <h3 class="title is-3">{{trans('pages.home.search.results', ["q"=>request()->get('q')])}}</h3>
                @else
                    <h3 class="title is-3">{{trans("products.categories.".$category->name)}}</h3>
                @endif
                <products></products>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    @parent
@endsection