@extends('layouts.main')
@section('body')
    <section class="section">
        <div class="columns is-multiline">
            <div class="column is-2 filter">
                <aside class="menu">
                    <p class="menu-label">
                        {{trans("pages.home.category")}}
                    </p>
                    <ul class="menu-list">
                        <li>
                            <a href="/">{{trans("links.home")}}</a>
                            <ul>
                                @foreach($categories as $category)
                                    @php($names = $category->getParentChain())
                                    @include('components.sidebar_entry', ['source'=>$names])
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column products is-10">
                <h3 class="title is-3">{{trans("pages.home.popular")}}</h3>
                <products load="popular"></products>
            </div>
        </div>
    </section>
@endsection