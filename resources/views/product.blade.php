@extends('layouts.main')
@section('body')
    <section class="section">
        <div class="columns is-multiline">
            <div class="column products is-10">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="{{url('/')}}">Shop</a></li>
                        @foreach($product->category->getParentChain() as $item)
                            <li><a href="{{url("/{$item->name}")}}">{{trans("products.categories.".$item->name)}}</a></li>
                        @endforeach
                        <li class="is-active"><a aria-current="page" href="{{url($product->category->name."/".$product->slug)}}">{{$product->name}}</a></li>
                    </ul>
                </nav>
                <div id="app" class="content section">
                    <div class="columns">
                        <div class="column is-5">
                            <figure class="image is-square">
                                <img src="{{url($product->title_image)}}"/>
                            </figure>
                            <div class="columns">
                                <div class="column">
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="content">
                                {!! $product->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    @parent
@endsection