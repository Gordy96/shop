@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Products
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section">
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Name</th>
                                        <th>Products</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($categories = $pagination->items())
                                @foreach($categories as $category)
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>
                                        @php($name = array_reverse($category->getConstructedName()))
                                        <a href="/admin/categories/{{$name[0]['id']}}">{{$name[0]['name']}}</a>
                                        @for($i = 1; $i < count($name); $i++)
                                            ≫<a href="/admin/categories/{{$name[$i]['id']}}">{{$name[$i]['name']}}</a>
                                        @endfor
                                    </td>
                                    <td>{{$category->products()->count()}}</td>
                                    <td class="is-narrow">
                                        <a class="button is-danger" href="/admin/categories/{{$category->id}}/delete">
                                            <span class="icon"><i class="fa fa-trash"></i></span>
                                            <span>Delete</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                        <div class="section">
                            <form method="post" action="/admin/categories">
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Parent category</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <div class="select">
                                                    <select name="parent">
                                                        <option value="0" selected>None</option>
                                                        @foreach(\App\ProductCategory::all() as $cat)
                                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Name</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="name" placeholder="e.g. Game">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Title en</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="name_en" placeholder="e.g. Game">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Title ru</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="name_ru" placeholder="e.g. Game">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <!-- Left empty for spacing -->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button type="submit" class="button is-primary">
                                                    <spam class="icon is-small">
                                                        <i class="fa fa-save"></i>
                                                    </spam>
                                                    <span>
                                                        Save
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection