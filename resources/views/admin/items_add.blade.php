@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Products
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section" id="app">
                            <product-select-form></product-select-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection