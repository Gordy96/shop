@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Create new product
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section" id="app">
                            <edit-product-form pid="{{$id}}"></edit-product-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection