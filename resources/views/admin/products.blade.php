@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Products
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section">
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($products = $pagination->items())
                                @foreach($products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td><a href="/admin/products/{{$product->id}}">{{$product->name}}</a></td>
                                    <td>
                                        @if($product->category)
                                        @php($name = array_reverse($product->category->getConstructedName()))
                                        <a href="/admin/categories/{{$name[0]['id']}}">{{$name[0]['name']}}</a>
                                        @for($i = 1; $i < count($name); $i++)
                                            ≫<a href="/admin/categories/{{$name[$i]['id']}}">{{$name[$i]['name']}}</a>
                                        @endfor
                                        @endif
                                    </td>
                                    <td class="is-narrow">
                                        <a class="button is-success" href="/admin/products/{{$product->id}}/publish">
                                            <span class="icon"><i class="fa fa-calendar-check-o"></i></span>
                                            <span>Publish</span>
                                        </a>
                                    </td>
                                    <td class="is-narrow">
                                        <a class="button is-danger" href="/admin/products/{{$product->id}}/delete">
                                            <span class="icon"><i class="fa fa-trash"></i></span>
                                            <span>Delete</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection