@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <a href="/admin" class="button">
                                <span class="icon">
                                    <i class="fa fa-backward"></i>
                                </span>
                            </a>&nbsp;
                            <span>Site settings</span>
                        </p>
                    </header>
                    <div class="card">
                        <div class="content section">
                            <form method="post">
                                @foreach(\App\SiteSetting::query()->whereNotIn('name', ['payment_method'])->get() as $entry)
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">{{$entry->name}}</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="{{$entry->name}}" placeholder="{{$entry->name}}" value="{{$entry->value}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <!-- Left empty for spacing -->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button class="button is-primary" type="submit">
                                                    Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection