@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <a href="/admin" class="button">
                                <span class="icon">
                                    <i class="fa fa-backward"></i>
                                </span>
                            </a>&nbsp;
                            <span>Tickets</span>
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="section">
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Theme</th>
                                    <th>Contact</th>
                                    <th>Category</th>
                                    <th>Status</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($products = $pagination->items())
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td><a href="/admin/support/{{$product->id}}">{{$product->theme}}</a></td>
                                        <td>
                                            {{$product->contact}}
                                        </td>
                                        <td>
                                            {{$product->category->name}}
                                        </td>
                                        <td>
                                            {{$product->status->name}}
                                        </td>
                                        <td class="is-narrow">
                                            @if($product->assignee_id === null)
                                            <a class="button is-info" href="/admin/support/{{$product->id}}/assign">
                                                <span class="icon"><i class="fa fa-user"></i></span>
                                                <span>Assign to me</span>
                                            </a>
                                            @endif
                                        </td>
                                        <td class="is-narrow">
                                            @if($product->status->name !== 'closed')
                                            <a class="button is-warning" href="/admin/support/{{$product->id}}/close">
                                                <span class="icon"><i class="fa fa-close"></i></span>
                                                <span>Close</span>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection