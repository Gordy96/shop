@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Products
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section">
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Name</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($attributes = $pagination->items())
                                @foreach($attributes as $attribute)
                                <tr>
                                    <td>{{$attribute->id}}</td>
                                    <td><a href="/admin/attributes/{{$attribute->id}}">{{$attribute->name}}</a></td>
                                    <td class="is-narrow">
                                        <a class="button is-danger" href="/admin/attributes/{{$attribute->id}}/delete">
                                            <span class="icon"><i class="fa fa-trash"></i></span>
                                            <span>Delete</span>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                        <div class="section">
                            <form method="post" action="/admin/attributes">
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Name</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="name" placeholder="e.g. Year">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Title en</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="name_en" placeholder="e.g. Year">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Title ru</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="name_ru" placeholder="e.g. Year">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <!-- Left empty for spacing -->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button type="submit" class="button is-primary">
                                                    <spam class="icon is-small">
                                                        <i class="fa fa-save"></i>
                                                    </spam>
                                                    <span>
                                                        Save
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection