@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Payment Systems
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content">
                            <table class="table is-fullwidth is-striped">
                                <tbody>
                                @foreach(\App\PaymentMethod::with(['accounts'])->get() as $item)
                                <tr>
                                    <td><a href="/admin/systems/{{$item->id}}">{{$item->name}}</a></td>
                                    <td>Active accounts : {{$item->accounts()->whereHas('status',function($q){$q->whereIn('name',['active','created']);})->count()}}/{{$item->accounts()->count()}}</td>
                                    @if(\App\SiteSetting::where('name','payment_method')->first()->value === $item->id)
                                    <td><a class="button is-small is-primary" href="#" disabled>Default</a></td>
                                    @else
                                    <td>
                                        <a class="button is-small is-primary" href="/admin/systems/{{$item->id}}/default">Make default</a>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection