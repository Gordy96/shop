@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div id="app" class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Language
                        </p>
                    </header>
                    <div class="card-body">
                        <div class="content section">
                            <div class="columns">
                                <div class="column">
                                    <div class="field is-horizontal">
                                        <div class="field-label">
                                            <label class="label">Filter by language</label>
                                        </div>
                                        <div class="field-body">
                                                <div class="field has-addons">
                                                    <div class="control">
                                                        <form method="get">
                                                            <button type="submit" class="button {{request()->get('locale') === 'en' ? 'is-primary' : ''}}">
                                                                <span>EN</span>
                                                            </button>
                                                            <input type="hidden" name="locale" value="en">
                                                        </form>
                                                    </div>
                                                    <div class="control">
                                                        <form method="get">
                                                            <button type="submit" class="button {{request()->get('locale') === 'ru' ? 'is-primary' : ''}}">
                                                                <span>RU</span>
                                                            </button>
                                                            <input type="hidden" name="locale" value="ru">
                                                        </form>
                                                    </div>
                                                    <div class="control">
                                                        <form method="get">
                                                            <a type="submit" class="button {{!request()->has('locale') ? 'is-primary' : ''}}" href="{{url()->current()}}">
                                                                <span>All</span>
                                                            </a>
                                                        </form>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <form method="post">
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Department</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                <input class="input" type="text" name="key" placeholder="Key">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <label class="label">Lang</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field is-narrow">
                                            <div class="control">
                                                <label class="radio">
                                                    <input type="radio" value="en" name="locale">
                                                    EN
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" value="ru" name="locale">
                                                    RU
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Value</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <textarea class="textarea" name="value" placeholder="Translation value"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <!-- Left empty for spacing -->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button type="submit" class="button is-primary">
                                                    Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                <tr>
                                    <th>key</th>
                                    @if(!request()->has('locale'))
                                    <th>locale</th>
                                    @endif
                                    <th>value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($items = $pagination->items())
                                @foreach($items as $item)
                                    <tr>
                                        <td>
                                            {{$item->key}}
                                        </td>
                                        @if(!request()->has('locale'))
                                        <td>
                                            {{$item->locale}}
                                        </td>
                                        @endif
                                        <td>
                                            <form method="post">
                                                <div class="field">
                                                    <div class="control">
                                                        <textarea class="textarea" name="value">{!! $item->value !!}</textarea>
                                                    </div>
                                                    <div class="control">
                                                        <button type="submit" class="button is-info">
                                                            <span>Update</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="id" value="{{$item->id}}"/>
                                                {{csrf_field()}}
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection