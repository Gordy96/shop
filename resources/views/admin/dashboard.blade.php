@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Orders
                        </p>
                    </header>
                    <div class="card-table">
                        <div id="app" class="content section">
                            <nav class="level">
                                <div class="level-item has-text-centered">
                                    <div>
                                        <p class="heading">Count</p>
                                        <p class="title">{{$total->count}}</p>
                                    </div>
                                </div>
                                <div class="level-item has-text-centered">
                                    <div>
                                        <p class="heading">Total money</p>
                                        <p class="title">{{$total->price ? $total->price : 0}}</p>
                                    </div>
                                </div>
                                <div class="level-item has-text-centered">
                                    <div>
                                        <p class="heading">Items sold</p>
                                        <p class="title">{{$total->product_count ? $total->product_count : 0}}</p>
                                    </div>
                                </div>
                            </nav>
                            <form method="get">
                                <div class="field has-addons has-addons-centered">
                                    <div class="control">
                                        <button type="submit" class="button {{request()->get('date_from') || request()->get('date_to') ? 'is-active is-primary' : ''}}">
                                            Custom
                                        </button>
                                    </div>
                                    <div class="control">
                                        <date-picker
                                                value="{{request()->get('date_from')}}"
                                                name="date_from"
                                                placeholder="Date from"
                                        >
                                        </date-picker>
                                    </div>
                                    <div class="control">
                                    <span class="button">
                                      -
                                    </span>
                                    </div>
                                    <div class="control">
                                        <date-picker
                                                value="{{request()->get('date_to')}}"
                                                name="date_to"
                                                placeholder="Date to"
                                        >
                                        </date-picker>
                                    </div>

                                    @php($btns = ['week'=>'Last week','14-days'=>'Last two weeks', 'month'=>'Last month', 'reset'=>'Reset'])
                                    @foreach($btns as $kind => $name)
                                        <div class="control">
                                            <a class="button {{request()->get('range') === $kind ? 'is-active is-primary' : ''}}" href="{{url()->current()}}{{$kind === 'reset' ? '' : "?range={$kind}"}}">
                                                <span>{{$name}}</span>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </form>
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                    <tr>
                                        <th>date</th>
                                        <th>pcs. sold</th>
                                        <th>Money get</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($orders = $pagination->items())
                                @foreach($orders as $order)
                                <tr>
                                    <td>
                                        {{$order->created_at}}
                                    </td>
                                    <td>
                                        {{$order->product_count}}
                                    </td>
                                    <td>
                                        {{$order->price}}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection