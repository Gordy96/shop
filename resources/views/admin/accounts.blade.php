@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Accounts
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content">
                            <table class="table is-fullwidth is-striped">
                                <tbody>
                                @foreach(\App\Account::with(['status','method', 'settings'])->get() as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td><a href="/admin/accounts/{{$item->id}}">{{$item->settings()->where('name','credentials')->first()->info->value->username}}</a></td>
                                    <td>{{$item->method->name}}</td>
                                    <td>{{$item->status->name}}</td>
                                    <td class="has-text-right">
                                    @if($item->status->name === 'created' || $item->status->name === 'active')
                                        <a class="button is-small is-warning" href="/admin/accounts/{{$item->id}}/toggle">Suspend</a>
                                    @else
                                        <a class="button is-small is-success" href="/admin/accounts/{{$item->id}}/toggle">Resume</a>
                                    @endif
                                        <a class="button is-small is-danger" href="/admin/accounts/{{$item->id}}/delete">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            New account
                        </p>
                    </header>
                    <div class="card-content">
                        @php($stocks = \App\PaymentMethod::all())
                        @if(count($stocks))
                            <div class="tabs-container">
                                <div class="tabs is-centered is-boxed">
                                    <ul>
                                        <li class="is-active">
                                            <a id="tab{{$stocks[0]->id}}" aria-controls="panel{{$stocks[0]->id}}" role="tab">
                                                {{$stocks[0]->name}}
                                            </a>
                                        </li>
                                        @for($i = 1; $i < count($stocks); $i++)
                                            <li>
                                                <a id="tab{{$stocks[$i]->id}}" aria-controls="panel{{$stocks[$i]->id}}" role="tab">
                                                    {{$stocks[$i]->name}}
                                                </a>
                                            </li>
                                        @endfor
                                    </ul>
                                </div>
                                <div class="tabs-content">
                                    <div id="panel{{$stocks[0]->id}}" aria-labelledby="tab{{$stocks[0]->id}}" role="tabpanel">
                                        @if(View::exists('admin.forms.'.strtolower($stocks[0]->name)))
                                            @include('admin.forms.'.strtolower($stocks[0]->name),['stock'=>$stocks[0]])
                                        @endif
                                    </div>
                                    @for($i = 1; $i < count($stocks); $i++)
                                        <div id="panel{{$stocks[$i]->id}}" aria-labelledby="tab{{$stocks[$i]->id}}" role="tabpanel" class="is-hidden">
                                            @if(View::exists('admin.forms.'.strtolower($stocks[$i]->name)))
                                                @include('admin.forms.'.strtolower($stocks[$i]->name),['stock'=>$stocks[$i]])
                                            @endif
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        var tabs = document.querySelectorAll('.tabs-container .tabs a[role=tab]');
        tabs.forEach(function(e){
            e.onclick = function(event){
                var self = this;
                tabs.forEach(function(t){
                    t === self ? t.parentNode.classList.add('is-active') : t.parentNode.classList.remove('is-active')
                });
                var current = document.getElementById(e.attributes['aria-controls'].value);
                for(var i = 0; i < current.parentNode.children.length; i++){
                    var c = current.parentNode.children[i];
                    if(c !== current)
                        c.classList.add('is-hidden');
                }
                current.classList.remove('is-hidden');
            }
        });
    </script>
@endsection