@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <a href="/admin/support" class="button">
                                <span class="icon">
                                    <i class="fa fa-backward"></i>
                                </span>
                            </a>&nbsp;
                            <span>Tickets</span>
                        </p>
                    </header>
                    <div class="card-body">
                        @include('components.support', ['ticket'=>$ticket])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection