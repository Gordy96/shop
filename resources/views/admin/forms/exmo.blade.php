<form action="/admin/accounts" method="post">
    <input type="hidden" name="method" value="{{$stock->id}}">
    <div class="field">
        <label class="label">Username</label>
        <div class="control has-icons-left has-icons-right">
            <input class="input" type="text" placeholder="Username" name="username" value="">
            <span class="icon is-small is-left">
                <i class="fa fa-user"></i>
            </span>
        </div>
    </div>
    <div class="field">
        <label class="label">Password</label>
        <div class="control has-icons-left has-icons-right">
            <input class="input" type="password" placeholder="Password" name="password" value="">
            <span class="icon is-small is-left">
                <i class="fa fa-lock"></i>
            </span>
        </div>
    </div>
    <div class="field">
        <label class="label">Key</label>
        <div class="control has-icons-left has-icons-right">
            <input class="input" type="text" placeholder="Key" name="key" value="">
            <span class="icon is-small is-left">
                <i class="fa fa-user"></i>
            </span>
        </div>
    </div>
    <div class="field">
        <label class="label">Secret</label>
        <div class="control has-icons-left has-icons-right">
            <input class="input" type="password" placeholder="Secret" name="secret" value="">
            <span class="icon is-small is-left">
                <i class="fa fa-lock"></i>
            </span>
        </div>
    </div>
    <div class="field">
        <label class="label">Proxy</label>
        <div class="control has-icons-left has-icons-right">
            <input class="input" type="text" placeholder="schema://[username:password@]address:port" name="proxy" value="">
            <span class="icon is-small is-left">
                <i class="fa fa-globe"></i>
            </span>
        </div>
    </div>
    <div class="field">
        <div class="control">
            <input type="submit" class="button is-link" value="Add"/>
        </div>
    </div>
    {{csrf_field()}}
</form>