@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column is-6">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <a href="/admin/systems" class="button">
                                <span class="icon">
                                    <i class="fa fa-backward"></i>
                                </span>
                            </a>&nbsp;
                            <span>{{$pm->name}}</span>
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section">
                            <form method="post">
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <!---->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <label class="label">Commission</label>
                                        </div>
                                        <div class="field">
                                            <label class="label">Min</label>
                                        </div>
                                        <div class="field">
                                            <label class="label">Max</label>
                                        </div>
                                    </div>
                                </div>
                                @foreach($pm->info as $name => $val)
                                    <div class="field is-horizontal">
                                        <div class="field-label is-normal">
                                            <label class="label">{{$name}}</label>
                                        </div>
                                        <div class="field-body">
                                            <div class="field">
                                                <div class="control">
                                                    <input class="input" type="text" placeholder="Commission" name="{{$name}}_commission" value="{{$val->commission}}">
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="control">
                                                    <input class="input" type="text" placeholder="Commission" name="{{$name}}_range[]" value="{{$val->range[0]}}">
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="control">
                                                    <input class="input" type="text" placeholder="Commission" name="{{$name}}_range[]" value="{{$val->range[1]}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <!---->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button class="button is-success" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection