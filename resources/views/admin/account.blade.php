@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <a href="/admin/accounts" class="button">
                                <span class="icon">
                                    <i class="fa fa-backward"></i>
                                </span>
                            </a>&nbsp;
                            <span>Account</span>
                        </p>
                    </header>
                    <div class="card">
                        <div class="content section">
                            @if($account->status->name === 'mail_confirmation')
                                <form method="post" action="/admin/accounts/{{$account->id}}/mail">
                                    <div class="field is-horizontal">
                                        <div class="field-label is-normal">
                                            <label class="label">Code</label>
                                        </div>
                                        <div class="field-body">
                                            <div class="field">
                                                <div class="control">
                                                    <input class="input" type="text" name="code" placeholder="Mail code" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field is-horizontal">
                                        <div class="field-label">
                                            <!-- Left empty for spacing -->
                                        </div>
                                        <div class="field-body">
                                            <div class="field">
                                                <div class="control">
                                                    <button class="button is-primary" type="submit">
                                                        Send
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                </form>
                            @endif
                            <form method="post">
                                @foreach($account->settings()->where('name', 'credentials')->first()->info->value as $key =>$value)
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">{{$key}}</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="{{$key}}" placeholder="{{$key}}" value="{{$value}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @php($proxy = $account->settings()->where('name', 'proxy')->first())
                                @if($proxy)
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">
                                            {{$proxy->name}}
                                        </label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <input class="input" type="text" name="{{$proxy->name}}" placeholder="{{$proxy->name}}" value="{{$proxy->info->value}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <!-- Left empty for spacing -->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button class="button is-primary" type="submit">
                                                    Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection