@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div id="app" class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Orders
                        </p>
                    </header>
                    <div class="card-body">
                        <div class="content section">
                            <form method="get">
                                <div class="columns">
                                    <div class="column is-3">
                                        <div class="field is-horizontal">
                                            <div class="field-label is-normal">
                                                <label class="label">Date</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field has-addons">
                                                    <div class="control">
                                                        <date-picker
                                                                value="{{request()->get('date_from')}}"
                                                                name="date_from"
                                                                placeholder="Date from"
                                                        >
                                                        </date-picker>
                                                    </div>
                                                    <div class="control">
                                                    <span class="button">
                                                      -
                                                    </span>
                                                    </div>
                                                    <div class="control">
                                                        <date-picker
                                                                value="{{request()->get('date_to')}}"
                                                                name="date_to"
                                                                placeholder="Date to"
                                                        >
                                                        </date-picker>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field is-horizontal">
                                            <div class="field-label is-normal">
                                                <label class="label">Price</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field has-addons">
                                                    <div class="control">
                                                        <input class="input" type="text" value="{{request()->get('price_from')}}" name="price_from" placeholder="Price from">
                                                    </div>
                                                    <div class="control">
                                                    <span class="button">
                                                      -
                                                    </span>
                                                    </div>
                                                    <div class="control">
                                                        <input class="input" type="text" value="{{request()->get('price_to')}}" name="price_to" placeholder="Price to">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column">
                                        <div class="field is-horizontal">
                                            <div class="field-label is-normal">
                                                <label class="label">Info</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field">
                                                    <p class="control">
                                                        <input class="input" value="{{request()->get('info')}}" type="text" name="info" placeholder="Payment info">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field is-horizontal">
                                            <div class="field-label is-normal">
                                                <label class="label">Product</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field">
                                                    <p class="control">
                                                        <input class="input" value="{{request()->get('product')}}" type="text" name="product" placeholder="Product info">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column">
                                        <div class="field is-horizontal">
                                            <div class="field-label is-normal">
                                                <label class="label">Payment</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field">
                                                    <p class="control">
                                                        <input class="input" value="{{request()->get('payment')}}" type="text" name="payment" placeholder="Payment info">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field is-horizontal">
                                            <div class="field-label is-normal">
                                                <label class="label">Status</label>
                                            </div>
                                            <div class="field-body">
                                                <div class="field is-narrow">
                                                    <div class="control">
                                                        <div class="select is-fullwidth">
                                                            <select name="status">
                                                                <option value="0" {{request()->get('status') === 0 ? 'selected' : '' }}>All</option>
                                                                @foreach(\App\OrderStatus::all() as $status)
                                                                    <option value="{{$status->id}}" {{request()->get('status') == $status->id ? 'selected' : '' }}>{{$status->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="column is-3">
                                        <div class="field is-horizontal">
                                            <div class="field-label">
                                                <!-- Left empty for spacing -->
                                            </div>
                                            <div class="field-body">
                                                <div class="field has-addons">
                                                    <div class="control">
                                                        <button class="button is-primary" type="submit">
                                                            Filter
                                                        </button>
                                                    </div>
                                                    <div class="control">
                                                        <a class="button" href="/admin/orders">
                                                            Reset
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Info</th>
                                        <th>Products</th>
                                        <th>Payment</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($orders = $pagination->items())
                                @foreach($orders as $order)
                                <tr>
                                    <td>
                                        <a href="/admin/orders/{{$order->id}}">{{$order->id}}</a>
                                    </td>
                                    <td>
                                        @if($order->info)
                                            <ul style="margin-top: 0">
                                                @foreach($order->info as $key => $val)
                                                    <li>{{$key}} : {{$val}}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </td>
                                    <td>
                                        @php($count = ($count = count($order->products)) > 3 ? 3 : $count)
                                        @for($i = 0; $i < $count; $i++)
                                            @php($product = $order->products[$i])
                                            {{count($product->data->items)}} x <a href="/admin/products/{{$product->id}}">{{$product->name}}</a>{{$count > 1 ? ',' : ''}}
                                        @endfor
                                        @if($count === 3 && ($diff = count($order->products) - $count) > 0)
                                            <span>+{{$diff}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{$order->payment->method->name}} (#{{$order->payment->info}})
                                    </td>
                                    <td>
                                        <b>{{$order->price}}</b>
                                    </td>
                                    <td>
                                        {{$order->status->name}}
                                    </td>
                                    <td>
                                        {{$order->created_at}}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="section">
                            {{$pagination->links('components.pagination.default')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection