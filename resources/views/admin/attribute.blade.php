@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Attribute: {{$attribute->name}}
                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section">
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($attribute->products as $product)
                                    <tr>
                                        <td><a href="/admin/products/{{$product->id}}">{{$product->name}}</a></td>
                                        <td>{{$product->data->value}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection