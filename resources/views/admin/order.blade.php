@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            <a href="/admin/orders" class="button">
                                <span class="icon">
                                    <i class="fa fa-backward"></i>
                                </span>
                            </a>&nbsp;
                            <span>Order</span>

                        </p>
                    </header>
                    <div class="card-table">
                        <div class="content section">
                            <h3 class="title is-3">Order #{{$order->id}}</h3>
                            @if($order->info)
                                <ul>
                                    @foreach($order->info as $key => $val)
                                        <li>{{$key}} : {{$val}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <h3 class="subtitle"><a href="{{url("/order/{$order->hash}")}}">{{url("/order/{$order->hash}")}}</a></h3>
                            <table class="table is-fullwidth is-striped is-hoverable">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>price</th>
                                        <th>Item</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($order->items as $item)
                                <tr>
                                    <td>
                                        <a href="/admin/products/{{$item->data->product->id}}">{{$item->data->product->name}}</a>
                                    </td>
                                    <td>
                                        {{$item->data->product->price}}
                                    </td>
                                    <td>
                                        {{$item->info}}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <form method="post">
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Status</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field has-addons">
                                            <div class="control">
                                                <div class="select">
                                                    <select name="status" value="{{$order->status_id}}">
                                                        @foreach(\App\OrderStatus::all() as $status)
                                                            <option value="{{$status->id}}" {{$status->id !== $order->status_id ? : 'selected'}}>{{$status->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control">
                                                <button type="submit" class="button is-primary">Set</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                    <footer class="card-footer">
                    </footer>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection