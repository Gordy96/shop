@extends('layouts.admin')
@section('body')
    <div class="column is-9 section">
        <div class="columns">
            <div class="column">
                <div class="card events-card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Payment instruction
                        </p>
                    </header>
                    <div class="card-body">
                        <div class="section">
                            <form method="post">
                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label">Content</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <textarea class="textarea" style="height: 400px" name="content">{!! file_get_contents(resource_path('/views/components/instruction.blade.php')) !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field is-horizontal">
                                    <div class="field-label">
                                        <!-- Left empty for spacing -->
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button type="submit" class="button is-primary">
                                                    <spam class="icon is-small">
                                                        <i class="fa fa-save"></i>
                                                    </spam>
                                                    <span>
                                                        Save
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection