@extends('layouts.checkout')
@section('body')
    <div class="container has-text-centered">
        <div class="box">
            <section class="section">
                <div class="content" style="max-height: 600px; overflow-y: scroll;">
                    @include('components.instruction')
                </div>
            </section>
        </div>
        <div class="has-text-centered">
            <p class="title">
                {{trans("pages.checkout.livecoin.number")}}
            </p>
            <p class="subtitle">
            </p>
            <form>
                <div class="field has-addons has-addons-centered">
                    <div class="control">
                        <input class="input is-medium" name="phone" type="text" placeholder="XXXXXXXXXXXX">
                    </div>
                    <div class="control">
                        <button class="button is-info is-medium is-inverted" type="submit">
                            <span>{{trans("pages.checkout.proceed")}}</span>
                        </button>
                    </div>
                </div>
                @foreach(request()->all() as $field => $value)
                    <input type="hidden" name="{{$field}}" value="{{$value}}"/>
                @endforeach
            </form>
        </div>
    </div>
@endsection