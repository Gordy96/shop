<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    @section('styles')
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    @show
    @include('components.translations', ["forbidden"=>['auth', 'admin', 'passwords']])
</head>
<body>
    <div id="app">
        <nav class="navbar is-dark">
            <div class="container">
                <div class="navbar-brand">
                    <a class="navbar-item" href="/">
                    </a>
                    <span class="navbar-burger burger" data-target="navbarMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                </div>
                <div id="navbarMenu" class="navbar-menu">
                    <div class="navbar-start searchbox">
                        <form method="get" action="/search">
                            <div class="field has-addons">
                                <div class="control is-expanded">
                                    <input class="input" name="q" type="search" placeholder="{{trans("pages.home.search.field")}}">
                                </div>
                                <div class="control">
                                    <button type="submit" class="button is-info">
                                        <span class="icon is-small">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="navbar-end">
                        <a class="navbar-item{{ends_with(url()->current(),"/") ? " is-active" : ""}}" href="/">
                            {{trans("links.home")}}
                        </a>
                        <a class="navbar-item{{ends_with(url()->current(),"/faq") ? " is-active" : ""}}" href="/faq">
                            {{trans("links.faq")}}
                        </a>
                        <a class="navbar-item{{ends_with(url()->current(),"/support") ? " is-active" : ""}}" href="/support">
                            {{trans("links.support")}}
                        </a>
                        @if(Auth::user())
                            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('content_manager') || Auth::user()->hasRole('support'))
                            <a class="navbar-item" href="/admin">
                                {{trans("links.admin")}}
                            </a>
                            @endif
                            <a class="navbar-item" href="/logout">
                                {{trans("links.logout")}}
                            </a>
                        @endif
                        <example></example>
                        <div class="navbar-item">
                            <div class="field has-addons">
                                <p class="control">
                                    <a class="button {{app()->getLocale() === 'en' ? 'is-primary' : ''}}" href="/lang/en">
                                        <span>EN</span>
                                    </a>
                                </p>
                                <p class="control">
                                    <a class="button {{app()->getLocale() === 'ru' ? 'is-primary' : ''}}" href="/lang/ru">
                                        <span>RU</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <main class="container is-fluid">
            @section('body')
            @show
        </main>
    </div>
    @section('scripts')
        <script type="application/javascript" src="{{mix('js/manifest.js')}}"></script>
        <script type="application/javascript" src="{{mix('js/vendor.js')}}"></script>
        <script type="application/javascript" src="{{mix('js/app.js')}}"></script>
        @if(session()->has('messages'))
        <script>
            @foreach(session()->get('messages') as $message)
            Vue.$notify({type: 'is-{{$message['type']}}', message: "{{$message['text']}}", position: 'is-top-right', hasIcon: true, queue: false, duration: 15000});
            @endforeach
        </script>
        @endif
        @if($errors->any())
        <script>
            @foreach($errors->all() as $error)
            Vue.$notify({type: 'is-danger', message: "{{$error}}", position: 'is-top-right', hasIcon: true, queue: false, duration: 15000});
            @endforeach
        </script>
        @endif
        <script>
            document.addEventListener('DOMContentLoaded', () => {

                // Get all "navbar-burger" elements
                const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

                // Check if there are any navbar burgers
                if ($navbarBurgers.length > 0) {

                    // Add a click event on each of them
                    $navbarBurgers.forEach( el => {
                        el.addEventListener('click', () => {

                            // Get the target from the "data-target" attribute
                            const target = el.dataset.target;
                            const $target = document.getElementById(target);

                            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                            el.classList.toggle('is-active');
                            $target.classList.toggle('is-active');

                        });
                    });
                }

            });
        </script>
    @show
</body>
</html>
