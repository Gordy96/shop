<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    @section('styles')
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    @show
</head>
<body>
<div id="app">
    <main class="">
        <section class="hero is-primary is-bold is-fullheight">
            <div class="hero-head">
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-brand">
                            <a class="navbar-item" href="/">
                                {{trans("links.home")}}
                            </a>
                        </div>
                        <div id="navbarMenuHeroB" class="navbar-menu">
                            <div class="navbar-start">
                                <a class="navbar-item" href="/support">
                                    {{trans("links.support")}}
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="hero-body">
                @section('body')
                @show
            </div>

            <div class="hero-foot">
                @section('footer')
                @show
            </div>
        </section>
    </main>
</div>
@section('scripts')
    <script type="application/javascript" src="{{mix('js/manifest.js')}}"></script>
    <script type="application/javascript" src="{{mix('js/vendor.js')}}"></script>
    <script type="application/javascript" src="{{mix('js/app.js')}}"></script>
    @if(session()->has('messages'))
        <script>
            @foreach(session()->get('messages') as $message)
            Vue.$notify({type: 'is-{{$message['type']}}', message: "{{$message['text']}}", position: 'is-top', hasIcon: true, queue: false, duration: 15000});
            @endforeach
        </script>
    @endif
@show
</body>
</html>
