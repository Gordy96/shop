<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    @section('styles')
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
        <style>
            html, body {
                font-size: 16px;
                line-height: 1.5;
                height: 100%;
                background-color: #ECF0F3;
            }
        </style>
    @show

    @include('components.translations', ["forbidden"=>[]])
</head>
<body>
<nav class="navbar is-white">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item brand-text" href="/admin">
                Bulma Admin
            </a>
            <div class="navbar-burger burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div id="navMenu" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/">
                    Home
                </a>
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                <a class="navbar-item" href="/admin/settings">
                    Settings
                </a>
                @endif
            </div>
            <div class="navbar-end">
                @if(Auth::user())
                <a class="navbar-item" href="/logout">
                    Logout
                </a>
                @endif
            </div>

        </div>
    </div>
</nav>
<div class="container is-fluid">
    <div class="columns">
        <div class="column is-3 section">
            <aside class="menu">
                <p class="menu-label">
                    Support
                </p>
                <ul class="menu-list">
                    @include('components.menu_entry', ['name'=>"Support", "link"=>"/admin/support"])
                </ul>
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin') || \Illuminate\Support\Facades\Auth::user()->hasRole('content_manager'))
                <p class="menu-label">
                    General
                </p>
                <ul class="menu-list">
                    @include('components.menu_entry', ['name'=>"Dashboard", "link"=>"/admin"])
                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    @include('components.menu_entry', ['name'=>"Payment systems", "link"=>"/admin/systems"])
                    @endif
                </ul>
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                <p class="menu-label">
                    Accounts
                </p>
                <ul class="menu-list">
                    @include('components.menu_entry', ['name'=>"All", "link"=>"/admin/accounts"])
                </ul>
                @endif
                <p class="menu-label">
                    Shop
                </p>
                <ul class="menu-list">
                    @component('components.menu_entry', ['name'=>"Products", "link"=>"/admin/products"])
                        @slot('sub_links')
                        @include('components.menu_entry', ['name'=>"New", "link"=>"/admin/products/new"])
                        @endslot
                    @endcomponent
                    @component('components.menu_entry', ['name'=>"Items", "link"=>"/admin/items"])
                        @slot('sub_links')
                                @include('components.menu_entry', ['name'=>"Add", "link"=>"/admin/items/add"])
                        @endslot
                    @endcomponent
                </ul>
                <p class="menu-label">
                    Attributes/Categories
                </p>
                <ul class="menu-list">
                    @include('components.menu_entry', ['name'=>"Attributes", "link"=>"/admin/attributes"])
                    @include('components.menu_entry', ['name'=>"Categories", "link"=>"/admin/categories"])
                </ul>
                <p class="menu-label">
                    Transactions
                </p>
                <ul class="menu-list">
                    @include('components.menu_entry', ['name'=>"Orders", "link"=>"/admin/orders"])
                </ul>
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <p class="menu-label">
                        Instruction
                    </p>
                    <ul class="menu-list">
                        @include('components.menu_entry', ['name'=>"Edit", "link"=>"/admin/instruction"])
                    </ul>
                    <p class="menu-label">
                        Language
                    </p>
                    <ul class="menu-list">
                        @include('components.menu_entry', ['name'=>"Edit", "link"=>"/admin/lang"])
                    </ul>
                @endif
                @endif
            </aside>
        </div>
        @section('body')
        @show
    </div>
</div>
@section('scripts')
    <script type="application/javascript" src="{{mix('js/manifest.js')}}"></script>
    <script type="application/javascript" src="{{mix('js/vendor.js')}}"></script>
    <script type="application/javascript" src="{{mix('js/admin.js')}}"></script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {

            // Get all "navbar-burger" elements
            const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

                // Add a click event on each of them
                $navbarBurgers.forEach( el => {
                    el.addEventListener('click', () => {

                        // Get the target from the "data-target" attribute
                        const target = el.dataset.target;
                        const $target = document.getElementById(target);

                        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                        el.classList.toggle('is-active');
                        $target.classList.toggle('is-active');

                    });
                });
            }

        });
    </script>
    @if(session()->has('messages'))
        <script>
            @foreach(session()->get('messages') as $message)
            Vue.$notify({type: 'is-{{$message['type']}}', message: "{{$message['text']}}", position: 'is-top-right', hasIcon: true, queue: false, duration: 15000});
            @endforeach
        </script>
    @endif
    @if($errors->any())
        <script>
            @foreach($errors->all() as $error)
            Vue.$notify({type: 'is-danger', message: "{{$error}}", position: 'is-top-right', hasIcon: true, queue: false, duration: 15000});
            @endforeach
        </script>
    @endif
@show
</body>
</html>
