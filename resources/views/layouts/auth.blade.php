<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title')</title>
    @section('styles')
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
    @show
</head>
<body>
    <div id="app">
        <nav class="navbar is-dark">
            <div class="container">
                <div class="navbar-brand">
                    <a class="navbar-item" href="/">
                    </a>
                    <span class="navbar-burger burger" data-target="navbarMenu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>
                <div id="navbarMenu" class="navbar-menu">
                    <div class="navbar-start searchbox">
                        <form method="get" action="/search">
                            <div class="field has-addons">
                                <div class="control has-icons-left has-icons-right is-expanded">
                                    <input class="input has-text-centered" name="q" type="search" placeholder="Search">
                                    <span class="icon is-small is-left">
                                            <i class="fa fa-search"></i>
                                        </span>
                                </div>
                                <div class="control">
                                    <button type="submit" class="button is-info">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="navbar-end">
                        <a class="navbar-item is-active" href="/">
                            Home
                        </a>
                        <a class="navbar-item" href="/faq">
                            FAQ
                        </a>
                        <a class="navbar-item" href="/support">
                            Support
                        </a>
                        @if(Auth::user())
                            @if(Auth::user()->hasRole('admin'))
                            <a class="navbar-item" href="/admin">
                                Admin panel
                            </a>
                            @endif
                            <a class="navbar-item" href="/logout">
                                Logout
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </nav>
        <main class="container is-fluid">
            @section('body')
            @show
        </main>
    </div>
    @section('scripts')
        <script type="application/javascript" src="{{mix('js/manifest.js')}}"></script>
        <script type="application/javascript" src="{{mix('js/vendor.js')}}"></script>
        <script type="application/javascript" src="{{mix('js/app.js')}}"></script>
        @if(session()->has('messages'))
        <script>
            @foreach(session()->get('messages') as $message)
            Vue.$notify({type: 'is-{{$message['type']}}', message: "{{$message['text']}}", position: 'is-top-right', hasIcon: true, queue: false, duration: 15000});
            @endforeach
        </script>
        @endif
        <script>
            document.addEventListener('DOMContentLoaded', () => {

                // Get all "navbar-burger" elements
                const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

                // Check if there are any navbar burgers
                if ($navbarBurgers.length > 0) {

                    // Add a click event on each of them
                    $navbarBurgers.forEach( el => {
                        el.addEventListener('click', () => {

                            // Get the target from the "data-target" attribute
                            const target = el.dataset.target;
                            const $target = document.getElementById(target);

                            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                            el.classList.toggle('is-active');
                            $target.classList.toggle('is-active');

                        });
                    });
                }

            });
        </script>
    @show
</body>
</html>
