@extends('layouts.main')
@section('body')
    <section class="section">
        <div class="columns">
            <div class="column is-4 is-offset-4">
                <login-prompt-component></login-prompt-component>
            </div>
        </div>
    </section>
@endsection