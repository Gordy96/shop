<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['guest']], function(){
    Route::group(['prefix'=>'login'], function(){
        Route::post('/', 'Auth\\LoginController@login');
        Route::get('/', 'Auth\\LoginController@showLoginForm')->name('login');
    });
});
Route::get('/lang/{ru}', function(\Illuminate\Http\Request $request, $lang){
    session()->put('locale', $lang);
    return redirect()->back();
});
Route::group(['middleware'=>['auth']], function(){
    Route::group(['prefix'=>'logout'], function(){
        Route::any('/', 'Auth\\LoginController@logout');
    });
});

Route::group(['middleware'=>['throttle:60,1']], function(){
    Route::group(['prefix'=>'support'], function(){
        Route::get('/', 'TicketController@ShowTicketCreationPage');
        Route::post('/', 'TicketController@CreateTicket');
        Route::group(['prefix'=>'{hash}'], function(){
            Route::get('/', 'TicketController@ShowTicket');
            Route::post('/', 'TicketController@SendMessage');
        });
    });
});

Route::get('/search', 'StaticController@SearchPage');
Route::get('/', 'StaticController@PopularPage')->name('home');

Route::group(['prefix'=>'checkout', 'middleware'=>['throttle:6,1800']], function(){
    Route::any('/{method}', 'ProductController@ConfirmOrder');
});

Route::group(['prefix'=>'order'], function(){
    Route::get('/{hash}', 'ProductController@GetOrder');
});

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:admin,content_manager,support']], function(){
    Route::get('/', 'Admin\\StaticController@ShowDashBoardPage');

    Route::group(['prefix'=>'support', 'middleware'=>[]], function(){
        Route::get('/', 'Admin\\TicketController@ShowTickets');
        Route::group(['prefix'=>'/{id}'], function() {
            Route::get('/', 'Admin\\TicketController@ShowTicket');
            Route::get('/assign', 'Admin\\TicketController@AssignTicket');
            Route::get('/close', 'Admin\\TicketController@CloseTicket');
            Route::post('/', 'Admin\\TicketController@SendMessage');
        });
    });

    Route::group(['middleware'=>['role:!support']], function(){

        Route::group(['prefix'=>'instruction', 'middleware'=>['role:admin']], function(){
            Route::get('/', 'Admin\\StaticController@ShowInstructionPage');
            Route::post('/', 'Admin\\StaticController@UpdateInstruction');
        });

        Route::group(['prefix'=>'lang', 'middleware'=>['role:admin']], function(){
            Route::get('/', 'Admin\\StaticController@ShowLanguageListPage');
            Route::post('/', 'Admin\\StaticController@UpdateLang');
        });

        Route::group(['prefix'=>'systems', 'middleware'=>['role:admin']], function(){
            Route::get('/', 'Admin\\StaticController@ShowPaymentMethodsPage');
            Route::group(['prefix'=>'/{id}'], function() {
                Route::any('/default', 'Admin\\StaticController@SetDefaultPaymentMethod');
                Route::get('/', 'Admin\\StaticController@ShowPaymentMethodPage');
                Route::post('/', 'Admin\\StaticController@UpdatePaymentMethodPage');
            });
        });

        Route::group(['prefix'=>'settings', 'middleware'=>['role:admin']], function(){
            Route::get('/', 'Admin\\StaticController@ShowSettingsPage');
            Route::post('/', 'Admin\\StaticController@UpdateSettings');
        });

        Route::group(['prefix'=>'accounts', 'middleware'=>['role:admin']], function(){
            Route::get('/', 'Admin\\StaticController@ShowAccountsPage');
            Route::post('/', 'Admin\\StaticController@CreateAccount');
            Route::group(['prefix'=>'/{id}'], function(){
                Route::get('/', 'Admin\\StaticController@ShowAccountPage');
                Route::post('/', 'Admin\\StaticController@UpdateAccount');
                Route::post('/mail', 'Admin\\StaticController@AccountMailConfirmation');
                Route::any('/toggle', 'Admin\\StaticController@ToggleAccount');
                Route::any('/delete', 'Admin\\StaticController@DeleteAccount');
            });
        });

        Route::group(['prefix'=>'products'], function(){
            Route::get('/', 'Admin\\StaticController@ShowProductsPage');
            Route::get('/new', 'Admin\\StaticController@ShowProductCreationPage');
            Route::group(['prefix'=>'/{id}'], function() {
                Route::any('/', 'Admin\\StaticController@ShowProductEditPage');
                Route::get('/delete', 'Admin\\StaticController@DeleteProduct');
                Route::get('/publish', 'Admin\\StaticController@PublishProduct');
            });
        });

        Route::group(['prefix'=>'attributes'], function(){
            Route::get('/', 'Admin\\StaticController@ShowAttributesPage');
            Route::post('/', 'Admin\\StaticController@CreateAttribute');
            Route::group(['prefix'=>'/{id}'], function(){
                Route::get('/', 'Admin\\StaticController@GetAttributeValues');
                Route::any('/delete', 'Admin\\StaticController@DeleteAttribute');
            });
        });

        Route::group(['prefix'=>'categories'], function(){
            Route::get('/', 'Admin\\StaticController@ShowCategoriesPage');
            Route::post('/', 'Admin\\StaticController@CreateCategory');
            Route::group(['prefix'=>'/{id}'], function(){
                Route::get('/', 'Admin\\StaticController@GetCategoryProducts');
                Route::any('/delete', 'Admin\\StaticController@DeleteCategory');
            });
        });

        Route::group(['prefix'=>'items'], function(){
            Route::get('/', 'Admin\\StaticController@ShowItemsPage');
            Route::get('/add', 'Admin\\StaticController@ShowAddItemsPage');
        });

        Route::group(['prefix'=>'orders'], function(){
            Route::get('/', 'Admin\\StaticController@ShowOrdersPage');
            Route::group(['prefix'=>'{id}'], function(){
                Route::get('/', 'Admin\\StaticController@ShowOrderPage');
                Route::post('/', 'Admin\\StaticController@UpdateOrder');
            });
        });
    });
});

Route::group(['prefix'=>'{category}'], function(){
    Route::get('/', 'StaticController@IndexPage');
    Route::get('/{slug}', 'StaticController@ProductPage');
});

Route::group(['prefix'=>'api'], function (){
    Route::group(['prefix'=>'v1'], function (){

        Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin,content_manager']], function (){
            Route::group(['prefix'=>'attributes'], function (){
                Route::get('/', 'Admin\\ApiController@GetAllAttributes');
                Route::group(['prefix'=>'{id}'], function (){
                    Route::get('/', 'Admin\\ApiController@GetAttributeValues');
                });
//                Route::post('/', 'ProductController@ConfirmOrder');
            });
            Route::group(['prefix'=>'categories'], function (){
                Route::get('/', 'Admin\\ApiController@GetAllCategories');
            });
            Route::group(['prefix'=>'products'], function (){
                Route::post('/', 'Admin\\ApiController@CreateProduct');
                Route::get('/', 'Admin\\ApiController@GetProducts');
                Route::group(['prefix'=>'{id}'], function (){
                    Route::get('/', 'Admin\\ApiController@GetProduct');
                    Route::group(['prefix'=>'items'], function (){
                        Route::get('/', 'Admin\\ApiController@GetProductItems');
                        Route::post('/', 'Admin\\ApiController@CreateItem');
                    });
                });
            });
            Route::group(['prefix'=>'media'], function (){
                Route::post('/', 'Admin\\ApiController@UploadMedia');
                Route::get('/', 'Admin\\ApiController@GetMedia');
                Route::group(['prefix'=>'{id}'], function (){
                    Route::get('/delete', 'Admin\\ApiController@DeleteMedia');
                });
            });
            Route::group(['prefix'=>'items'], function (){
                Route::post('/', 'Admin\\ApiController@CreateItem');
                Route::get('/', 'Admin\\ApiController@GetItems');
                Route::group(['prefix'=>'{id}'], function (){
                    Route::get('/', 'Admin\\ApiController@GetItem');
                    Route::get('/delete', 'Admin\\ApiController@DeleteItem');
                });
            });
        });


        Route::group(['prefix'=>'cart'], function (){
            Route::get('/', 'ProductController@Cart');
            Route::group(['prefix'=>'{id}'], function (){
                Route::any('/', 'ProductController@AddToCart');
                Route::get('/remove', 'ProductController@RemoveFromCart');
            });
            Route::post('/', 'ProductController@ConfirmOrder');
        });
        Route::group(['prefix'=>'checkout'], function (){
            Route::any('/methods', 'ProductController@GetAvailableMethods');
        });
        Route::group(['prefix'=>'products'], function (){
            Route::get('/filters', 'ProductController@ShowProductFilters');
            Route::get('/', 'ProductController@ShowProducts');
            Route::post('/{id}', 'ProductController@AddToCart');
        });
    });
});