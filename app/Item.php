<?php

namespace App;

use App\Helpers\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'status_id',
        'info',
        'product_id'
    ];

//    protected $casts = [
//        'info'=>'object'
//    ];

    public function status(){
        return $this->belongsTo(ItemStatus::class, 'status_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
