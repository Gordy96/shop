<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $fillable = [
        'status_id',
        'method_id',
        'description',
        'last_transaction_date'
    ];

    public $dates = [
        'created_at',
        'updated_at'
    ];

    public function status(){
        return $this->belongsTo(AccountStatus::class, 'status_id');
    }

    public function method(){
        return $this->belongsTo(PaymentMethod::class, 'method_id');
    }

    public function settings(){
        return $this->belongsToMany(
            AccountSetting::class,
            'account_account_setting',
            'account_id',
            'setting_id',
            'id',
            'id'
        )->withPivot(['value', 'parameters','created_at', 'updated_at'])->using(AccountSettingPivot::class)->as('info');
    }

    public function transactions(){
        return $this->hasMany(AccountTransaction::class, 'account_id');
    }
}
