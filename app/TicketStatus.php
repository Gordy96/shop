<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function tickets(){
        return $this->hasMany(Ticket::class,'status_id');
    }
}
