<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProductItemPivot extends Pivot
{
    protected $fillable = [
        'order_id',
        'item_id',
        'product_id'
    ];

    public function items(){
        return $this->belongsToMany(Item::class, 'product_order',
            'product_id',
            'item_id',
            'product_id')->where('order_id', $this->order_id);
    }
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
