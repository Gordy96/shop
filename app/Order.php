<?php

namespace App;

use App\Helpers\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    protected $fillable = [
        'price',
        'info',
        'status_id'
    ];

    protected $casts = [
        'info'=>'object'
    ];

    public function status(){
        return $this->belongsTo(OrderStatus::class, 'status_id');
    }

    /**
     * @return BelongsToMany
     */
    public function products(){
        return $this->belongsToMany(Product::class, 'product_order', 'order_id', 'product_id')
            ->using(OrderProductItemPivot::class)
            ->withPivot(['product_id', 'item_id'])
            ->groupBy(['order_id','product_id'])
            ->as('data')
            ->with([
                'data.items',
                'data.items.status'
            ]);
    }

    /**
     * @return BelongsToMany
     */
    public function items(){
        return $this->belongsToMany(Item::class, 'product_order', 'order_id', 'item_id')
            ->using(OrderProductItemPivot::class)
            ->withPivot(['product_id', 'item_id'])
            ->as('data')
            ->with([
                'data.product'
            ]);
    }

    public function payment(){
        return $this->hasOne(Payment::class, 'order_id');
    }
}
