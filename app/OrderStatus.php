<?php

namespace App;

use App\Helpers\Eloquent\Model;

class OrderStatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function orders(){
        return $this->hasMany(Order::class, 'status_id');
    }
}
