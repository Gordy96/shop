<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountStatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public function accounts(){
        return $this->hasMany(Account::class,'status_id');
    }
}
