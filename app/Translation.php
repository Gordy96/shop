<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = [
        'locale',
        'key',
        'value'
    ];

    public $timestamps = false;

    private static function _fetch(&$ary, $keys, $val) {
        $keys ?
            static::_fetch($ary[array_shift($keys)], $keys, $val) :
            $ary = $val;
    }

    public static function Get($group, $locale){
        $res = static::query()
            ->where('key', 'LIKE', "{$group}%")
            ->where('locale', $locale)
            ->get();
        if($res->count() === 1 && $res->first()->key === $group){
            return $res->first()->value;
        }
        $temp = [];
        foreach($res->pluck('value', 'key') as $k => $v)
            static::_fetch($temp, explode('.', $k), $v);
        return isset($temp[$group]) ? $temp[$group] : null;
    }
}
