<?php

namespace App;

use App\Helpers\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'order_id',
        'method_id',
        'status_id',
        'account_id',
        'info',
        'url'
    ];

    protected $casts = [
        'info'=>'object'
    ];

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function status(){
        return $this->belongsTo(PaymentStatus::class, 'status_id');
    }

    public function account(){
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function method(){
        return $this->belongsTo(PaymentMethod::class, 'method_id');
    }
}
