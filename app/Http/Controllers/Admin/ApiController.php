<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\JsonResponseFactory;
use App\Item;
use App\Media;
use App\Product;
use App\ProductAttribute;
use App\ProductCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function GetAllAttributes(Request $request)
    {
        $attributes = ProductAttribute::all();
        return JsonResponseFactory::success($attributes);
    }

    public function GetAttributeValues(Request $request, $id)
    {
        $values = ProductAttribute::query()
            ->join('product_product_attribute', 'attribute_id', '=', 'product_attributes.id')
            ->groupBy(['value'])
            ->where('attribute_id', $id)->get(['value']);
        return JsonResponseFactory::success($values->pluck('value'));
    }

    public function GetAllCategories(Request $request)
    {
        $categories = ProductCategory::all();
        return JsonResponseFactory::success($categories);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|ProductCategory
     */
    private function getProductCategory($id)
    {
        return ProductCategory::query()->findOrFail($id);
    }

    public function CreateProduct(Request $request)
    {
        if($request->has('id') && ($product = Product::find($request->get('id')))){
            $product->update([
                'price'=>$request->get('price'),
                'name_en'=>$request->get('name_en'),
                'name_ru'=>$request->get('name_ru'),
                'description'=>$request->get('description'),
                'rating'=>0
            ]);
            $product->category()->associate($request->get('category'));
            $product->save();
        } else {
            $prod_cat = $this->getProductCategory($request->get('category'));
            $product = $prod_cat->products()->save(new Product([
                'price'=>$request->get('price'),
                'name_en'=>$request->get('name_en'),
                'name_ru'=>$request->get('name_ru'),
                'description'=>$request->get('description'),
                'rating'=>0
            ]));
        }
        $re_attributes = $request->get('attributes');
        $db_attributes = ProductAttribute::query()->whereIn('name', array_keys($re_attributes))->get();
        $attributes = [];
        foreach($db_attributes as $attribute)
            $attributes[$attribute->id] = ['value'=>$re_attributes[$attribute->name]];
        $product->attributes()->sync($attributes);
        $product->media()->saveMany(Media::whereIn('id', $request->get('media'))->get());
        $product->load(['attributes', 'category', 'media', 'media.type']);
        return JsonResponseFactory::success($product);
    }

    public function GetProduct(Request $request, $id)
    {
        $product = Product::with(['attributes', 'category', 'media', 'media.type'])->findOrFail($id);
        $product->makeVisible(['name_en','name_ru']);
        return JsonResponseFactory::success($product);
    }

    public function GetProductItems(Request $request, $id)
    {
        Product::with(['items', 'items.status'])->findOrFail($id);
        $items = Item::with(['status', 'product'])
            ->where('product_id', $id)
            ->orderBy('id');

        if($request->has('status')){
            $items->whereHas('status', function($builder) use ($request) {
                if(is_array($request->get('status')))
                    $builder->whereIn('name', $request->get('status'));
                else
                    $builder->where('name', $request->get('status'));
            });
        }

        $items = $items->paginate();
        return JsonResponseFactory::success($items);
    }

    public function GetProducts(Request $request)
    {
        if($request->has('only')){
            $only = is_array($request->get('only')) ? $request->get('only') : [$request->get('only')];
            $product = Product::select($only);
            if(count($only) == 1){
                $product = $product->pluck($only[0]);
            } else if (count($only) == 2){
                $product = $product->pluck($only[1], $only[0]);
            } else {
                $product = $product->get();
            }
        } elseif ($request->has('q')) {
            $product = Product::with(['attributes', 'category', 'media', 'media.type'])
                ->where('name_ru', 'like', "%{$request->get('q')}%")
                ->orWhere('name_en', 'like', "%{$request->get('q')}%")
                ->get();
        }
        else {
            $product = Product::with(['attributes', 'category', 'media', 'media.type'])->paginate();
        }
        return JsonResponseFactory::success($product);
    }

    public function GetMedia(Request $request)
    {
        return JsonResponseFactory::success(Media::with(['type'])->paginate());
    }

    public function UploadMedia(Request $request)
    {
        $response = [];
        if($request->media){
            foreach($request->media as $media){
                $filename = $media->store('/public');
                $m = Media::create([
                    'name' => '/'.str_ireplace("public", "storage", $filename),
                    'type_id'=>1
                ]);
                array_push($response, $m);
            }
        }
        return JsonResponseFactory::success($response);
    }

    public function DeleteMedia(Request $request, $id)
    {
        $media = Media::with(['type'])->findOrFail($id);
        unlink(public_path($media->name));
        return $media->delete() ? JsonResponseFactory::success(null) : JsonResponseFactory::fail(null);
    }

    public function GetItems(Request $request)
    {
        $items = Item::with(['status', 'product'])->orderBy('id');
        if($request->has('status')){
            $items->whereHas('status', function($builder) use ($request) {
                if(is_array($request->get('status')))
                    $builder->whereIn('name', $request->get('status'));
                else
                    $builder->where('name', $request->get('status'));
            });
        }
        $items = $items->paginate();
        return JsonResponseFactory::success($items);
    }

    public function GetItem(Request $request, $id)
    {
        return JsonResponseFactory::success(Item::with(['status', 'product'])->findOrFail($id));
    }

    public function CreateItem(Request $request, $id = null)
    {
        $id = $id ? $id : $request->get('product');
        if($id && $request->has('info')){
            $info = $request->get('info');
            if(is_array($info)){
                $item = new Collection();
                foreach($info as $i)
                    $item->push(Item::create([
                        'product_id'=>$id,
                        'info'=>$i,
                        'status_id'=>1
                    ]));
            } else {
                $item = Item::create([
                    'product_id'=>$id,
                    'info'=>$info,
                    'status_id'=>1
                ]);
            }
            $item->load([
                'status',
                'product'
            ]);
            return JsonResponseFactory::success($item);
        }
        return JsonResponseFactory::fail([]);
    }

    public function DeleteItem(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        return $item->delete() ? JsonResponseFactory::success(null) : JsonResponseFactory::fail(null);
    }
}
