<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\AccountSetting;
use App\AccountStatus;
use App\Exceptions\NotEnoughItemsException;
use App\Helpers\Eloquent\Builder;
use App\Item;
use App\Jobs\AccountKeeper;
use App\Order;
use App\OrderStatus;
use App\PaymentMethod;
use App\PaymentStatus;
use App\Product;
use App\ProductAttribute;
use App\ProductCategory;
use App\SiteSetting;
use App\Translation;
use Carbon\Carbon;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class StaticController extends Controller
{

    use ValidatesRequests;

    public function ShowPaymentMethodsPage(Request $request)
    {
        return view('admin.payment_systems');
    }

    public function SetDefaultPaymentMethod(Request $request, $id)
    {
        $pm = PaymentMethod::findOrFail($id);
        SiteSetting::where('name', 'payment_method')->update(['value'=>$pm->id]);
        return redirect()->back();
    }

    public function ShowAccountsPage(Request $request)
    {
        return view('admin.accounts');
    }

    /**
     * @param $id
     * @return Account
     */
    private function getAccount($id){
        return Account::with(['status','method','settings'])->findOrFail($id);
    }

    public function ShowAccountPage(Request $request, $id)
    {
        $account = $this->getAccount($id);
        return view('admin.account', ["account"=>$account]);
    }

    public function AccountMailConfirmation(Request $request, $id)
    {
        $account = $this->getAccount($id);
        $api = $account->method->createApiInstance($account);
        $result = $api->Auth();

        if(!$result)
            return redirect()->back()->with('messages', [
                'type'=>'danger',
                'text'=>'Wrong code or other error'
            ]);
        else{
            $this->ToggleAccount($request, $account->id);
            return redirect()->back();
        }
    }

    public function UpdateAccount(Request $request, $id)
    {
        $account = $this->getAccount($id);
        $filtered = collect($this->validate($request, [
            'username'=>'required|min:6',
            'password'=>'required|min:8',
            'secret'=>'sometimes|required',
            'key'=>'sometimes|required_with:secret',
            'proxy'=>'required'
        ]));
        $filtered = collect($filtered);
        $account->settings()->updateExistingPivot($account->settings()->where('name','credentials')->first()->id, [
            'value'=>$filtered->except(['proxy'])
        ]);
        if($filtered->has('proxy')){
            $pr = $account->settings()->where('name','proxy')->first();
            $account->settings()->updateExistingPivot($pr->id, [
                'value'=>$filtered->get('proxy')
            ]);
        }
        return redirect()->back()->with('messages', [
            ['type'=>'info', 'text'=>'Account updated']
        ]);
    }

    public function ToggleAccount(Request $request, $id)
    {
        $account = $this->getAccount($id);
        if(!in_array($account->status->name, ['active', 'created'])){
            $account->status()->associate(AccountStatus::where('name','created')->first());
            $jobs_found = DB::table('jobs')->where('queue', AccountKeeper::getQueueName($account))->pluck('id');
            if(!count($jobs_found)){
                AccountKeeper::createJob($account)->delay(1);
            }

        } else {
            $account->status()->associate(AccountStatus::where('name','suspended')->first());
            $jobs_found = DB::table('jobs')->where('queue', AccountKeeper::getQueueName($account))->pluck('id');
            if(count($jobs_found)){
                DB::table('jobs')->whereIn('id', $jobs_found)->delete();
            }
        }
        $account->save();
        return redirect()->back();
    }

    public function DeleteAccount(Request $request, $id)
    {
        $account = $this->getAccount($id);
        $account->delete();
        return redirect()->back();
    }

    public function CreateAccount(Request $request)
    {
        $filtered = collect($this->validate($request, [
            'method'=>'required|exists:payment_methods,id',
            'username'=>'required|min:6',
            'password'=>'required|min:8',
            'secret'=>'sometimes|required',
            'key'=>'sometimes|required_with:secret',
            'proxy'=>'required'
        ]));
        $pm = PaymentMethod::findOrFail($filtered->get('method'));
        $account = Account::create(['method_id'=>$pm->id]);
        $account->settings()->attach(AccountSetting::where('name','credentials')->first()->id, [
            'value'=>$filtered->except(['proxy','method'])
        ]);
        if($filtered->has('proxy')){
            $pr = AccountSetting::where('name','proxy')->first();
            $account->settings()->attach($pr->id, [
                'value'=>$filtered->get('proxy')
            ]);
        }
        AccountKeeper::createJob($account)->delay(1);
        return redirect()->back()->with('messages', [
            ['type'=>'info', 'text'=>'Account created']
        ]);
    }

    public function ShowProductCreationPage(Request $request)
    {
        return view('admin.new_product');
    }

    public function ShowProductsPage(Request $request)
    {
        $products = Product::with(['category'])->paginate();
        return view('admin.products', ['pagination'=>$products]);
    }

    public function ShowProductEditPage(Request $request, $id)
    {
        return view('admin.product', ['id'=>$id]);
    }

    public function PublishProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        if($product->published_at)
            return redirect()->back()->with('messages', [
                ['type'=>'danger', 'text'=>'Product is already published']
            ]);
        $product->update([
            'published_at'=>Carbon::now()
        ]);
        return redirect()->back()->with('messages', [
            ['type'=>'info', 'text'=>'Product published']
        ]);
    }

    public function DeleteProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        if($product->delete())
            return redirect()->back()->with('messages', [
                ['type'=>'info', 'text'=>'Product published']
            ]);
        else
            return redirect()->back()->with('messages', [
                ['type'=>'info', 'text'=>'Cannot delete product']
            ]);
    }

    public function ShowAttributesPage(Request $request)
    {
        $attributes = ProductAttribute::paginate();
        return view('admin.attributes', ['pagination'=>$attributes]);
    }

    public function GetAttributeValues(Request $request, $id)
    {
        $attributes = ProductAttribute::with(['products'])->findOrFail($id);
        return view('admin.attribute', [
            'attribute'=>$attributes
        ]);
    }

    public function CreateAttribute(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'name_en'=>'required',
            'name_ru'=>'required'
        ]);
        $attribute = ProductAttribute::create([
            'name'=>$request->get('name')
        ]);

        $msgs  = [
        ];
        if($attribute){
            Translation::create([
                'locale'=>'en',
                'key'=>'products.filters.'.$request->get('name'),
                'value'=>$request->get('name_en')
            ]);
            Translation::create([
                'locale'=>'ru',
                'key'=>'products.filters.'.$request->get('name'),
                'value'=>$request->get('name_ru')
            ]);
            array_push($msgs, ['type'=>'info', 'text'=>'Attribute created']);
        }
        else
            array_push($msgs, ['type'=>'danger', 'text'=>'Cannot create attribute']);

        return redirect()->back()->with('messages', $msgs);
    }

    public function DeleteAttribute(Request $request, $id)
    {
        $attribute = ProductAttribute::findOrFail($id);

        $msgs  = [
        ];
        $name = $attribute->name;
        if($attribute->delete()){
            Translation::query()->where('key', 'products.filters.'.$name);
            array_push($msgs, ['type'=>'info', 'text'=>'Attribute ' . $name .' deleted']);
        }
        else
            array_push($msgs, ['type'=>'danger', 'text'=>'Cannot delete ' . $name]);

        return redirect()->back()->with('messages', $msgs);
    }

    public function ShowCategoriesPage(Request $request)
    {
        $attributes = ProductCategory::paginate();
        return view('admin.categories', ['pagination'=>$attributes]);
    }

    public function GetCategoryProducts(Request $request, $id)
    {
        $attributes = ProductCategory::with(['products'])->findOrFail($id);
        return view('admin.category', [
            'category'=>$attributes
        ]);
    }

    public function CreateCategory(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'name_en'=>'required',
            'name_ru'=>'required',
            'parent'=>'sometimes',
        ]);
        $cr = [
            'name'=>$request->get('name')
        ];
        if($request->has('parent') && $request->get('parent') != 0){
            $parent = ProductCategory::findOrFail($request->get('parent'));
            $cr['parent_id'] = $parent->id;
        }

        $category = ProductCategory::create($cr);

        $msgs  = [
        ];
        if($category){
            Translation::create([
                'locale'=>'en',
                'key'=>'products.categories.'.$request->get('name'),
                'value'=>$request->get('name_en')
            ]);
            Translation::create([
                'locale'=>'ru',
                'key'=>'products.categories.'.$request->get('name'),
                'value'=>$request->get('name_ru')
            ]);
            array_push($msgs, ['type'=>'info', 'text'=>'Category created']);
        }
        else
            array_push($msgs, ['type'=>'danger', 'text'=>'Cannot create category']);

        return redirect()->back()->with('messages', $msgs);
    }

    public function DeleteCategory(Request $request, $id)
    {
        $category = ProductCategory::findOrFail($id);

        $msgs  = [
        ];
        $name = $category->name;
        if($category->delete()){
            Translation::query()->where('key', 'products.filters.'.$name);
            array_push($msgs, ['type'=>'info', 'text'=>'Category ' . $name .' deleted']);
        }
        else
            array_push($msgs, ['type'=>'danger', 'text'=>'Cannot delete ' . $name]);

        return redirect()->back()->with('messages', $msgs);
    }

    public function ShowItemsPage(Request $request)
    {
        return view('admin.items');
    }

    public function ShowAddItemsPage(Request $request)
    {
        return view('admin.items_add');
    }

    public function ShowOrdersPage(Request $request)
    {
        $orders = Order::with(['payment', 'payment.method']);

        if($request->get('payment')){
            $orders->whereHas('payment', function(Builder $builder) use ($request) {
                $builder->whereIn('method_id', PaymentMethod::query()->where('name', 'like', "%{$request->get('payment')}%")->pluck('id'))
                    ->orWhere('info', 'like', "%{$request->get('payment')}%");
            });
        }

        if($request->get('price_from') && $request->get('price_to')){
            $orders->whereBetween('price', [$request->get('price_from'), $request->get('price_to')]);
        } else if($request->get('price_from')){
            $orders->where('price', ">=", $request->get('price_from'));
        } else if($request->get('price_to')){
            $orders->where('price', "<=", $request->get('price_to'));
        }

        if($request->get('date_from') && $request->get('date_to')){
            $orders->whereBetween('created_at', [$request->get('date_from'), "{$request->get('date_to')} 23:59:59"]);
        } else if($request->get('date_from')){
            $orders->where('created_at', ">=", $request->get('date_from'));
        } else if($request->get('date_to')){
            $orders->where('created_at', "<=", "{$request->get('date_to')} 23:59:59");
        }

        if($request->get('status'))
            $orders->where('status_id', $request->get('status'));

        if($request->get('product')){
            $orders->whereHas('products', function(Builder $builder) use ($request) {
                $builder->where('name', 'like', "%{$request->get('product')}%");
            });
        }

        if($request->get('info')){
            $orders->where('info', "like", "%{$request->get('info')}%");
        }

        $orders = $orders->latest()->paginate();
        return view('admin.orders', [
            'pagination'=>$orders
        ]);
    }

    public function ShowOrderPage(Request $request, $id)
    {
        $order = Order::with(['products'])->find($id);
        return view('admin.order', [
            'order'=>$order
        ]);
    }

    public function UpdateOrder(Request $request, $id)
    {
        $filtered = $this->validate($request, [
            'status'=>'required|exists:order_statuses,id'
        ]);
        $order = Order::find($id);

        if($filtered['status'] == OrderStatus::query()->where('name', 'complete')->firstOrFail()->id){
            $all_available = true;
            $temp = [];
            foreach($order->items as $item){
                $all_available &= in_array($item->status->name, ['available']);
                array_push($temp, $item->id);
            }
            if($all_available){
                Item::query()->whereIn('id', $temp)->update(['status_id'=>3]);
            } else {
                try{
                    DB::transaction(function() use ($order) {
                        $ids = [];
                        foreach($order->products as $product){
                            $items = Item::query()
                                ->where('product_id', $product->id)
                                ->where('status_id', 1)
                                ->limit($product->data->items->count())
                                ->get();
                            if($items->count() !== $product->data->items->count())
                                throw new NotEnoughItemsException();
                            foreach($items as $item)
                                $ids[$item->id] = ["product_id"=>$product->id];
                        }
                        Item::query()->whereIn('id', array_keys($ids))->update(['status_id'=>3]);
                        $order->items()->sync($ids);
                        $order->status()->associate(OrderStatus::query()->where('name', 'complete')->first());
                        $order->save();
                        $order->payment->status()->associate(PaymentStatus::where('name','complete')->first());
                        $order->payment->save();
                        return true;
                    });
                } catch(\Throwable $exception){
                    return redirect()->back()->with('messages', [
                        [
                            'type'=>'danger',
                            'text'=>'Not all items are present'
                        ]
                    ]);
                }
                return redirect()->back()->with('messages', [
                    [
                        'type'=>'danger',
                        'text'=>'Not all items are present'
                    ]
                ]);
            }
        } else if($filtered['status'] == OrderStatus::query()->where('name', 'cancelled')->firstOrFail()->id){
            $temp = [];
            foreach($order->items as $item){
                array_push($temp, $item->id);
            }

            Item::query()->whereIn('id', $temp)->update(['status_id'=>1]);
        }

        $order->status_id = $filtered['status'];
        $order->save();
        $ps = PaymentStatus::query()->where('name', $order->status->name)->first();
        if($ps){
            $order->payment->status_id = $ps->id;
            $order->payment->save();
        }
        return redirect()->back()->with('messages', [
            [
                'type'=>'info',
                'text'=>'Order updated'
            ]
        ]);
    }

    public function ShowPaymentMethodPage(Request $request, $id){
        $pm = PaymentMethod::query()->findOrFail($id);
        return view('admin.payment_system', ['pm'=>$pm]);
    }

    public function UpdatePaymentMethodPage(Request $request, $id){
        $pm = PaymentMethod::query()->findOrFail($id);
        $new = [];
        foreach($request->except('_token') as $key => $value){
            $name = explode('_', $key);
            if(!isset($new[$name[0]]))
                $new[$name[0]] = [];

            $new[$name[0]][$name[1]] = $value;
        }
        $pm->info = $new;
        if($pm->save())
            return redirect()->back()->with('messages', [
                ['type'=>'info', 'text'=>'Updated']
            ]);
        else
            return redirect()->back()->with('messages', [
                ['type'=>'info', 'text'=>'Cannot update']
            ]);
    }

    public function ShowDashBoardPage(Request $request){

        if(Auth::user()->hasRole('support'))
            return redirect('/admin/support');

        $now = Carbon::now();
        $time_ranges = [
            'week'=>[with(clone $now)->subDays(7), $now],
            '14-days'=>[with(clone $now)->subDays(14), $now],
            'month'=>[with(clone $now)->subMonth(), $now]
        ];

        $base = DB::table('orders')
            ->join('order_statuses', 'orders.status_id', 'order_statuses.id')
            ->join('product_order', 'product_order.order_id', 'orders.id')
            ->whereIn('order_statuses.name', [
                'complete'
            ]);
        if($request->get('date_from') && $request->get('date_to')){
            $base->whereBetween('orders.created_at', [$request->get('date_from'), "{$request->get('date_to')} 23:59:59"]);
        } elseif($request->get('date_from')){
            $base->where('orders.created_at', ">=", $request->get('date_from'));
        } elseif($request->get('date_to')){
            $base->where('orders.created_at', "<=", "{$request->get('date_to')} 23:59:59");
        } elseif($request->has('range') && isset($time_ranges[$request->get('range')])){
            $base->whereBetween('orders.created_at', $time_ranges[$request->get('range')]);
        }
        $sub = clone $base;
        $sub = $sub->groupBy(DB::raw('orders.id'))
            ->mergeBindings($base)
            ->selectRaw('orders.*,count(product_order.product_id) as product_count');

        $total = clone $base;
        $total = $total
            ->mergeBindings($base)
            ->selectRaw('orders.price, count(product_order.product_id) as product_count, count(orders.id) as count')
            ->groupBy('orders.id');
        $total = DB::table(DB::raw("({$total->toSql()}) as t"))
            ->mergeBindings($total)
            ->selectRaw('sum(t.price) as price, sum(t.product_count) as product_count, count(t.count) as count');

        $orders = DB::table(DB::raw("({$sub->toSql()}) as t"))
            ->mergeBindings($sub)
            ->selectRaw('t.*, sum(t.price) as price, sum(t.product_count) as product_count, date(t.created_at) as created_at')
            ->groupBy(DB::raw('date(t.created_at)'))
            ->latest();
        return view('admin.dashboard', ['pagination'=>$orders->paginate(), 'total'=>$total->first()]);
    }

    public function ShowSettingsPage(Request $request)
    {
        return view('admin.settings');
    }

    public function UpdateSettings(Request $request)
    {
        $all = SiteSetting::all();
        foreach($all as $setting){
            if($request->has($setting->name)){
                $setting->value = $request->get($setting->name);
                $setting->save();
            }
        }
        return redirect()->back();
    }

    public function ShowInstructionPage(Request $request){
        return view('admin.instruction');
    }

    public function UpdateInstruction(Request $request){
        if($request->has('content')){
            file_put_contents(resource_path('/views/components/instruction.blade.php'), $request->get('content'));
        }
        return redirect()->back();
    }

    public function ShowLanguageListPage(Request $request){
        $pagination = Translation::query();
        if($request->has('locale'))
            $pagination->where('locale', $request->get('locale'));
        return view('admin.lang', ['pagination'=>$pagination->paginate()]);
    }

    public function UpdateLang(Request $request){
        if(!$request->has('id') && $request->has('key') && $request->has('locale')){
            $new = Translation::query()->create([
                'value'=>$request->get('value'),
                'locale'=>$request->get('locale'),
                'key'=>$request->get('key'),
            ]);
            if($new){
                Artisan::call('cache:clear');
                return redirect()->back()->with('messages', [
                    ['type'=>'info', 'text'=>'Entry added']
                ]);
            }
            else
                return redirect()->back()->with('messages', [
                    ['type'=>'info', 'text'=>'Cannot add entry']
                ]);
        }
        try{
            $this->validate($request, [
                'id'=>'required|exists:translations,id',
                'value'=>'required'
            ]);
        } catch(ValidationException $e){
            return redirect()->back()->withErrors($e->errors());
        }
        Artisan::call('cache:clear');
        Translation::query()->findOrFail($request->get('id'))->update(['value'=>$request->get('value')]);
        return redirect()->back()->with('messages', [
            ['type'=>'info', 'text'=>'Entry updated']
        ]);
    }
}
