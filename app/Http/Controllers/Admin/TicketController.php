<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Ticket;
use App\TicketCategory;
use App\TicketMessage;
use App\TicketStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class TicketController extends Controller
{

    public function ShowTicket(Request $request, $id){
        $ticket = Ticket::findOrFail($id);
        return view('admin.ticket', ['ticket'=>$ticket]);
    }

    public function ShowTickets(Request $request){
        $tickets = Ticket::query()->orderBy('status_id')->latest()->paginate();
        return view('admin.tickets', ['pagination'=>$tickets]);
    }

    public function CloseTicket(Request $request, $id){
        $ticket = Ticket::findOrFail($id);
        $ticket->status()->associate(TicketStatus::query()->where('name','closed')->first());
        $ticket->save();
        return redirect()->back()->with('messages', [
            [
                'type'=>'info',
                'text'=>'Ticket closed'
            ]
        ]);
    }

    public function AssignTicket(Request $request, $id){
        $ticket = Ticket::findOrFail($id);
        $ticket->assignee_id = Auth::user()->id;
        $ticket->save();
        return redirect()->back()->with('messages', [
            [
                'type'=>'info',
                'text'=>'Ticket assigned'
            ]
        ]);
    }

    public function SendMessage(Request $request, $id){
        try{
            $this->validate($request, [
                'text'=>'required',
            ]);
        } catch(ValidationException $e){
            return redirect()->back()->withErrors($e->errors());
        }

        $ticket = Ticket::findOrFail($id);
        $ticket->messages()->save(new TicketMessage([
            'text'=>$request->get('text'),
            'user_id'=>Auth::user()->id
        ]));
        $ticket->status()->associate(TicketStatus::query()->where('name','answered')->first());
        $ticket->save();
        return redirect()->back();
    }
}
