<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class StaticController extends Controller
{
    public function IndexPage(Request $request, $c = null){
        $category = ProductCategory::where('name', $c)->firstOrFail();
        if($category->products()->count())
            return view('products', ['category'=>$category]);
        elseif($category->sub_categories()->count()){
            if($request->has('q'))
                return redirect('/'.$c);
            return view('category', ['category'=>$category]);
        } else {
            abort(404);
        }
    }

    public function PopularPage(Request $request){
        $products = Product::query()
            ->orderByDesc('rating')
            ->orderByDesc('created_at')
            ->get();
        $categories = ProductCategory::query()->whereHas('products', function(Builder $builder) use ($products) {
            $builder->whereIn('id', $products->pluck('id'));
        })->get();
        return view('title', ['categories'=>$categories, ]);
    }

    public function SearchPage(Request $request){
        $search = ProductCategory::query()->whereHas('products', function(Builder &$builder) use ($request){
            ProductController::queryProducts($request, $builder);
        })->get();
        return view('products', ['search'=>$search]);
    }

    public function ProductPage(Request $request, $category, $slug)
    {
        $category = ProductCategory::query()->where('name', $category)->firstOrFail();
        $product = $category->products()->where('slug', $slug)->firstOrFail();
        return view('product', ['product'=>$product]);
    }
}
