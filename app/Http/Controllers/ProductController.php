<?php

namespace App\Http\Controllers;

use App\Helpers\ApiErrorFactory;
use App\Helpers\JsonResponseFactory;
use App\Helpers\Payments\Platforms\LiveCoinApi;
use App\ItemStatus;
use App\Jobs\ReleaseOrderItems;
use App\Order;
use App\Payment;
use App\PaymentMethod;
use App\PaymentStatus;
use App\Product;
use App\ProductAttribute;
use App\ProductCategory;
use App\ProductRate;
use App\SiteSetting;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * @param Request $request
     * @param $product_id int
     * @return \Illuminate\Http\JsonResponse
     */
    public function RateProduct(Request $request, $product_id)
    {
        $product = Product::find($product_id);
        if(!$product)
            return JsonResponseFactory::fail([
                ApiErrorFactory::tr(ApiErrorFactory::MODEL_NOT_FOUND)
            ]);
        $product->rates()->save(new ProductRate([
            'value'=>$request->get('value')
        ]));
        return JsonResponseFactory::success(null);
    }

    public function RemoveFromCart(Request $request, $id){
        if(session()->has("cart.items.{$id}")){
            session()->forget("cart.items.{$id}");
        }
        return JsonResponseFactory::success(null);
    }

    /**
     * @param Request $request
     * @param $id int
     * @return \Illuminate\Http\JsonResponse
     */
    public function AddToCart(Request $request, $id)
    {
        $q = 1;
        if(session()->has("cart.items.{$id}")){
            $q += session()->get("cart.items.{$id}")['quantity'];
        }


        $q = $request->get('quantity', $q);
        $q = $q <= 0 ? 1 : $q;
        $product = Product::whereHas('items', function(&$q) {
            $q->where('status_id', 1);
        }, '>=', $q)->find($id);
        if(!$product)
            return JsonResponseFactory::fail([ApiErrorFactory::tr(ApiErrorFactory::MODEL_NOT_ENOUGH_ITEMS)]);

        session()->put("cart.items.{$product->id}", [
            'product'=>$product->id,
            'quantity'=>$q
        ]);
//        if(session()->has("cart.form")){
//            session()->forget("cart.form");
//            $payment = session()->get("cart.payment");
//            if($payment){
//                $payment->status()->attach(PaymentStatus::where('name', 'cancelled')->first()->id);
//                foreach($payment->order->products as $product)
//                    $product->data->item->save(['status_id'=>ItemStatus::where('name', 'available')->first()->id]);
//            }
//            session()->forget("cart.payment");
//        }

        return $this->Cart($request);
    }

    private function getCartContent(){
        $cart = session()->get('cart.items', []);
        foreach(array_keys($cart) as $i){
            $prod = Product::whereHas('items', function(&$q){
                $q->where('status_id', 1);
            }, '>=', $cart[$i]['quantity'])->find($i);
            if(!$prod){
                $prod = Product::find($i);
                $cart[$i]['error'] = ApiErrorFactory::tr(ApiErrorFactory::MODEL_NOT_ENOUGH_ITEMS);
            }
            if($prod)
                $cart[$i]['product'] = $prod;
        }
        return $cart;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function Cart(Request $request)
    {
        $items = $this->getCartContent();
        $total = 0;
        foreach($items as $item){
            if(isset($item['product']))
                $total += $item['product']->price * $item['quantity'];
        }
        return JsonResponseFactory::success([
            'items'=>$items,
            'total'=>$total
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ShowProducts(Request $request)
    {
        $sort = $request->get('sort');
        $products = Product::with(['category'])->whereHas('items', function(Builder $builder){
            $builder->where('status_id', 1);
        }, '>=', 1);
        $field = 'created_at';
        if(strpos($sort, 'popular') !== FALSE){
            $products = $products->withCount('orders');
            $field = 'orders_count';
        } else if (strpos($sort, 'price') !== FALSE){
            $field = 'price';
        } else if (strpos($sort, 'rating') !== FALSE){
            $field = 'rating';
        }

        $dir = strpos($sort, '-') === FALSE ? 'ASC' : 'DESC';
        if(($category = $request->get('category')))
            $products->whereHas('category', function(Builder $builder) use ($category){
                $builder->where('name', $category);
            });

        $this->filterProducts($request, $products);

        $products = $products->orderBy($field, $dir)->paginate();
        return JsonResponseFactory::success([
            'products'=>$products
        ]);
    }

    public function ShowProductFilters(Request $request){
        if($request->get('category')){
            $products = Product::whereHas('category', function($builder) use ($request) {
                $builder->where('name', $request->get('category'));
            })->whereHas('items', function ($builder){
                $builder->where('status_id', 1);
            })->pluck('id');
            $filters = $this->getAvailableFilters($products);
        } else {
            $filters = $this->getAllFilters();
        }

        return JsonResponseFactory::success([
            'filters'=>$filters
        ]);
    }

    /**
     * @return PaymentMethod
     */
    private function getDefaultPaymentMethod(){
        $def = SiteSetting::where('name', 'payment_method')->first();
        $method = PaymentMethod::find($def->value);
        return $method;
    }

    public function GetAvailableMethods(Request $request)
    {
        $items = $this->getCartContent();
        $total = 0;
        foreach($items as $item){
            if(isset($item['error']))
                return JsonResponseFactory::fail([ApiErrorFactory::tr(ApiErrorFactory::MODEL_NOT_ENOUGH_ITEMS)]);
            $total += $item['product']->price * $item['quantity'];
        }
        if($total <= 0)
            return JsonResponseFactory::fail([trans("errors.api.cart.empty")]);
        $method = $this->getDefaultPaymentMethod();
        $temp = [];
        $methods = $method->getMethods();
        $min = 32768;
        $max = -32768;
        foreach($methods as $name => $m){
            $prc = $total + (isset($m->commission) ? $total * ($m->commission / 100) : 0);
            if(isset($m->range)){
                if( $this->in_between($prc, $m->range))
                    $temp[$name] = $methods->{$name};
                $min = $m->range[0] < $min ? $m->range[0] : $min;
                $max = $m->range[1] > $max ? $m->range[1] : $max;
            }
        }
        return count(array_keys($temp))
            ? JsonResponseFactory::success($temp)
            : JsonResponseFactory::fail([trans("errors.api.invalid_total." . ($total > $max ? "above" : ($total < $min ? "below" : "fatal")))]);
    }

    private function in_between($number, array $range)
    {
        return count($range) == 2 && $number >= $range[0] && $number <= $range[1];
    }

    public function ConfirmOrder(Request $request, $m){

        $method = $this->getDefaultPaymentMethod();
        $account = $method->accounts()->inRandomOrder()->whereHas('status', function($builder) {
            $builder->where('name', 'active');
        })->first();
        if(!$account)
            return redirect()->back()->with('messages', [
                [
                    'type'=>'danger',
                    'text'=>'Something went wrong please contact our support'
                ]
            ]);
        $api = $method->createApiInstance($account);

        $request->merge([
            'method'=>$api->GetMethodIdByName($m),
            'method_name'=>$m
        ]);

        if($api instanceof LiveCoinApi && !$request->has('phone')){
            return view('livecoin_checkout');
        }

        try{
            return DB::transaction(function() use ($method, $api, $account, $m){
                $items = $this->getCartContent();
                $total = 0;
                foreach($items as $item){
                    if(isset($item['error']))
                        return abort(400);
                    $total += $item['product']->price * $item['quantity'];
                }
                $methods = $method->getMethods();
                request()->merge([
                    'receive_amount'=>$total
                ]);
                $ototal = $total;
                $total = isset($methods->{$m}) && isset($methods->{$m}->commission) ? ($total / (1 - ($methods->{$m}->commission / 100))) : 0;
                if($total <= 0 || (!isset($methods->{$m}) || (isset($methods->{$m}->range) && !$this->in_between($total, $methods->{$m}->range))))
                    throw new \Exception(ApiErrorFactory::tr(ApiErrorFactory::TOTAL_CANT_BE_NULL));
                $session = session();

                $total = round($total, 2);

                $order = new Order([
                    'price'=>$ototal,
                    'status_id'=>1,
                    'info'=>request()->only(['email', 'method_name'])
                ]);
                $order->save();
                foreach($items as $product){
                    for($i = 0; $i < $product['quantity']; $i++){
                        $item = $product['product']->items()->where('status_id', 1)->first();
                        $item->status_id = 2;
                        $item->save();
                        $order->products()->attach($product['product']->id, ['item_id'=>$item->id]);
                    }
                }

                $res = $api->GetPaymentLink('RUR', $total);

                $pinf = $api->GetTransactionDetails();
                $raw = microtime(true) . $pinf;
                $order->hash = hash_hmac('sha256', $raw, Hash::make($raw));
                $order->save();

                $payment = new Payment([
                    'order_id'=>$order->id,
                    'method_id'=>$method->id,
                    'account_id'=>$account->id,
                    'status_id'=>1,
                    'info'=>$pinf,
                    'url'=>$api->GetButton($res instanceof RedirectResponse ? $res->getTargetUrl() : $res)
                ]);

                $payment->save();
                if($res === false)
                    throw new \Exception(ApiErrorFactory::tr(ApiErrorFactory::ERROR_FATAL));
                $session->forget('cart');
                $session->put('cart.payment', $payment->id);
                ReleaseOrderItems::createJob($order);
                return redirect('/order/'.$order->hash);
            });
        } catch (\Throwable $exception){
            Log::error($exception);
            return redirect()->back()->with('messages', [
                [
                    'type'=>'danger',
                    'text'=>'Something went wrong please contact our support'
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     * @param Builder $builder
     */
    private function filterProducts(Request $request, Builder &$builder)
    {
        $attribute_names = ProductAttribute::pluck('name');
        $filters = $request->only($attribute_names->all());
        $product_table = (new Product())->getTable();
        if(count($filters)){
            $product_attribute_table = (new ProductAttribute())->getTable();
            $builder->join('product_product_attribute', $product_table.'.id', 'product_id')
                ->join($product_attribute_table, $product_attribute_table.'.id', 'attribute_id');
            foreach($filters as $filter_name => $filter_value)
                $builder->where($product_attribute_table.'.name', $filter_name)
                    ->whereIn('product_product_attribute.value', is_array($filter_value) ? $filter_value : [$filter_value]);
            $builder->select($product_table.'.*');
        }
        static::queryProducts($request, $builder);
    }

    public static function queryProducts(Request $request, Builder &$builder)
    {
        if($request->has('q')){
            $q = explode(' ', $request->get('q'));
            $q = implode('%', $q);
            $builder->where('products.name_ru', 'like', "%{$q}%");
            $builder->orWhere('products.name_en', 'like', "%{$q}%");
        }
    }

    /**
     * @param $products Collection|array
     * @return \Illuminate\Support\Collection
     */
    private function getAvailableFilters($products)
    {
        $attributes = DB::table((new ProductAttribute())->getTable())
            ->join('product_product_attribute', 'product_attributes.id','product_product_attribute.attribute_id')
            ->groupBy(['value','attribute_id'])
            ->whereIn('product_id', $products)
            ->get(['value','name']);
        $temp = [];
        $attributes->map(function($element) use (&$temp){
            if(isset($temp[$element->name])){
                array_push($temp[$element->name], $element->value);
                $temp[$element->name] = array_unique($temp[$element->name]);
            } else {
                $temp[$element->name] = [$element->value];
            }
        });
        return collect($temp);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getAllFilters()
    {
        $attributes = DB::table((new ProductAttribute())->getTable())
            ->join('product_product_attribute', 'product_attributes.id','product_product_attribute.attribute_id')
            ->groupBy(['value','attribute_id'])
            ->get(['value','name']);
        $temp = [];
        $attributes->map(function($element) use (&$temp){
            if(isset($temp[$element->name])){
                array_push($temp[$element->name], $element->value);
                $temp[$element->name] = array_unique($temp[$element->name]);
            } else {
                $temp[$element->name] = [$element->value];
            }
        });
        return collect($temp);
    }

    public function GetOrder(Request $request, $hash)
    {
        $order = Order::query()->where('hash', $hash)->firstOrFail();
        if($order->status->name === 'complete'){

            $tmpHandle = tmpfile();
            $metaDatas = stream_get_meta_data($tmpHandle);
            $tmpFilename = $metaDatas['uri'];

            $file = new \SplFileObject($tmpFilename, "w");
//            $file->fputcsv(['title', 'value']);
            foreach($order->products as $product)
                foreach($product->data->items as $item){
                    $return = $file->fputcsv([$product->name, $item->info]);
                    if($return !== FALSE && 0 === $file->fseek(-1, SEEK_CUR)) {
                        $file->fwrite("\r\n");
                    }
                }

//            return response()->download($file);
//            return response(file_get_contents($tmpFilename), 200, [
//                'Content-Type'=>'application/octet-stream',
//                'Content-Disposition'=>'attachment; filename="'.str_random().'.csv"',
//                'Content-Length'=>$file->getSize()
//            ]);
            return view('checkout', [
                'payment'=>$order->payment,
                'data'=>file_get_contents($tmpFilename)
            ]);
        }
        if($order->status->name !== 'complete' && !$order->payment->url)
            abort(500);
        if(in_array($order->status->name, ['error']))
            abort(404);
        return view('checkout', [
            'payment'=>$order->payment
        ]);
    }
}

