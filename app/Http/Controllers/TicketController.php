<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketCategory;
use App\TicketMessage;
use App\TicketStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class TicketController extends Controller
{
    public function CreateTicket(Request $request){
        try{
            $this->validate($request, [
                'contact'=>'required|min:3',
                'theme'=>'required|min:3',
                'first_message'=>'required|min:16',
                'category'=>'required|exists:ticket_categories,id'
            ]);
        } catch(ValidationException $e){
            return redirect()->back()->withErrors($e->errors());
        }
        $ticket_category = TicketCategory::query()->findOrFail($request->get('category'));
        $status = TicketStatus::where('name', 'new')->first();
        if($status){
            $hash = hash_hmac('sha256', microtime(true).$request->get('contact'), microtime(true));
            $ticket = $ticket_category->tickets()->save(new Ticket([
                'status_id'=>$status->id,
                'theme'=>$request->get('theme'),
                'key'=>$hash,
                'contact'=>$request->get('contact')
            ]));

            $ticket->messages()->save(new TicketMessage([
                'text'=>$request->get('first_message')
            ]));

            return redirect('/support/'.$hash);
        }

        return redirect()->back();
    }

    public function ShowTicketCreationPage(Request $request){
        return view('support.ticket_new');
    }

    public function ShowTicket(Request $request, $hash){
        $ticket = Ticket::query()->where('key', ($hash))->firstOrFail();
        return view('support.ticket', ['ticket'=>$ticket, "hash"=>$hash]);
    }

    public function SendMessage(Request $request, $hash){
        try{
            $this->validate($request, [
                'text'=>'required',
            ]);
        } catch(ValidationException $e){
            return redirect()->back()->withErrors($e->errors());
        }

        $ticket = Ticket::query()->where('key', ($hash))->firstOrFail();
        $ticket->messages()->save(new TicketMessage([
            'text'=>$request->get('text')
        ]));
        $ticket->status()->associate(TicketStatus::query()->where('name', 'asked')->first());
        $ticket->save();
        return redirect()->back();
    }
}
