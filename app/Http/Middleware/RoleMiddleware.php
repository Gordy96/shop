<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  $parameters
     * @throws AuthenticationException
     * @return mixed
     */
    public function handle($request, Closure $next, ...$parameters)
    {
        $passes = false;
        foreach($parameters as $parameter){
            $neg = false;
            if(starts_with($parameter, '!')){
                $parameter = substr($parameter, 1);
                $neg = true;
            }
            if(Auth::user()->hasRole($parameter))
                $passes = $neg ? false : true;
            elseif($neg)
                $passes = $neg;
        }
        if(!$passes)
            abort(404);
        return $next($request);
    }
}
