<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{
    protected $fillable = [
        'product_id',
        'value',
        'user_info'
    ];

    protected $casts = [
        'user_info'=>'object'
    ];

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
