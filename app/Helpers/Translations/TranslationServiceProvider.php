<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 2018-07-15
 * Time: 15:10
 */

namespace App\Helpers\Translations;


class TranslationServiceProvider extends \Illuminate\Translation\TranslationServiceProvider
{
    /**
     * Register the translation line loader.
     *
     * @return void
     */
    protected function registerLoader()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new Loader($app['files'], $app['path.lang']);
        });
    }
}