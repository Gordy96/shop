<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 2018-07-15
 * Time: 15:14
 */

namespace App\Helpers\Translations;

use App\Translation;
use Illuminate\Support\Facades\Cache;
use Illuminate\Translation\FileLoader;

class Loader extends FileLoader
{

    /**
     * Load the messages for the given locale.
     *
     * @param  string  $locale
     * @param  string  $group
     * @param  string  $namespace
     * @return array
     */
    public function load($locale, $group, $namespace = null)
    {
        if ($namespace !== null && $namespace !== '*') {
            return $this->loadNamespaced($locale, $group, $namespace);
        }

        return Cache::remember("locale.fragments.{$locale}.{$group}", 60,
            function () use ($group, $locale) {
                return Translation::Get($group, $locale);
            });
    }
}