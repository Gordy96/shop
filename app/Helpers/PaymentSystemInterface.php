<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 27/06/2018
 * Time: 17:14
 */

namespace App\Helpers;


use App\AccountTransaction;

interface PaymentSystemInterface
{
    /**
     * @param $currency string
     * @param $amount float
     * @return mixed
     */
    public function GetPaymentLink($currency, $amount);

    /**
     * @param $currency string
     * @param $amount float
     * @param $info mixed
     * @param $from integer
     * @return AccountTransaction|null
     */
    public function CheckTransactionCompletion($currency, $amount, $info, $from);

    /**
     * @return array
     */
    public function GetTransactionDetails();

    public function GetMethodIdByName($name);

    public function GetButton($src);
}