<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/06/2018
 * Time: 12:50
 */

namespace App\Helpers;


class ApiErrorFactory
{
    const MODEL_NOT_FOUND           = 'err_not_found';
    const MODEL_NOT_ENOUGH_ITEMS    = 'err_not_enough_items';
    const TOTAL_CANT_BE_NULL        = 'err_cart_empty';
    const ERROR_FATAL               = 'err_fatal';

    public static function tr($err){
        return $err;
    }
}