<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 01/06/2018
 * Time: 11:59
 */

namespace App\Helpers\Payments\Platforms;


use App\Account;
use Carbon\Carbon;
use GuzzleHttp\Cookie\CookieJar;

interface StockApiInterface
{
    /**
     * StockApiInterface constructor.
     * @param Account|string $account
     */
    public function __construct($account);

    /**
     * @return array
     */
    public function getBalances();

    /**
     * @param $callback \Closure
     * @return array
     */
    public function getTransactionHistory(\Closure $callback);

    public function getRates();

    public function saveState();

    /**
     * @return mixed
     */
    public function topUp();

    public function Auth();
}