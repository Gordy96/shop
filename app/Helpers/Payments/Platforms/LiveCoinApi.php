<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 10/06/2018
 * Time: 00:29
 */

namespace App\Helpers\Payments\Platforms;


use App\Account;
use App\AccountSetting;
use App\AccountStatus;
use App\AccountTransaction;
use App\Helpers\Captcha\ApiErrorException;
use App\Helpers\Captcha\ImgToTextCaptcha;
use App\Helpers\Exceptions\BadAuthException;
use App\Helpers\Exceptions\WrongCaptchaException;
use App\Helpers\PaymentSystemInterface;
use App\Helpers\RandomUserAgent;
use App\SiteSetting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\ClientException;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\TransferStats;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Log;
use \phpseclib\Crypt\RSA;
use \phpseclib\Math\BigInteger;

class LiveCoinApi implements StockApiInterface, PaymentSystemInterface
{

    use ValidatesRequests;

    const base = 'https://api.livecoin.net/';

    /**
     * @var Client
     */
    private $client = null;

    /**
     * @var Account|null
     */

    private $account = null;

    /**
     * @var string
     */
    private $key = '';

    /**
     * @var string
     */
    private $secret = '';

    /**
     * @var string
     */
    private $email = null;

    /**
     * @var string
     */
    private $password = null;

    /**
     * @var string
     */
    private $ua = '';

    /**
     * @var object
     */
    private $state = null;

    private $currencies = [
        'RUR'=>'RUB'
    ];

    private $reverse_currencies;

    private $phone = null;

    /**
     * LiveCoinApi constructor.
     * @param $account
     */

    public function __construct($account)
    {
        $defs = [
            'base_uri'=>static::base,
            'verify'=>false,
            'cookies'=>true,
            'headers'=>[
            ]
        ];

        $this->reverse_currencies = array_flip($this->currencies);

        if($account instanceof Account){
            $this->account = $account;

            $ua = $account->settings()->where('name', 'user-agent')->first();
            if(!$ua){
                $ua = RandomUserAgent::getRandomUserAgent();
                $setting = AccountSetting::where('name', 'user-agent')->first();
                $account->settings()->attach($setting->id, [
                    'value'=>$ua
                ]);
            } else {
                $ua = $ua->info->value;
            }

            $defs['headers']['User-Agent'] = $ua;
            $this->ua = $ua;

            $creds = $account->settings()->where('name', 'credentials')->first()->info->value;
            $this->key = $creds->key;
            $this->secret = $creds->secret;
            $this->email = $creds->username;
            $this->password = $creds->password;
            $proxy = $account->settings()->where('name', 'proxy')->first();

            if($proxy)
                $defs['proxy'] = $proxy->info->value;

            if(($cookies = $account->settings()->where('name', 'cookies')->first()) && $cookies->info->value) {
                $defs['cookies'] = $this->setCookies($cookies->info->value);
            }
            $this->getAccountState();
        } elseif(is_string($account)){
            $defs['proxy'] = $account;
        }

        $this->client = new Client($defs);
    }

    private function getAccountState($force = false){
        if($this->account){
            if($this->state && !$force)
                return $this->state;
            if(($state = $this->account->settings()->where('name', 'state')->first()) && $state->info->value) {
                $this->state = $state->info->value;
                return $this->state;
            }
        }
        return null;
    }

    private function setAccountState($object){
        if(($state = AccountSetting::where('name', 'state')->first())) {
            $this->account->settings()->sync([
                $state->id=>[
                    'value'=>json_encode($object)
                ]
            ], false);
        }
        $this->state = $this->getAccountState(true);
    }

    private function setCookies($cookies)
    {
        $c = new CookieJar();
        foreach ($cookies as $cookie)
            $c->setCookie(
                new SetCookie([
                    'Name'     => $cookie->name,
                    'Value'    => $cookie->value,
                    'Domain'   => $cookie->domain,
                    'Path'     => $cookie->path,
                    'Expires'  => $cookie->expires
                ])
            );
        return $c;
    }

    private function post($uri, $params = [])
    {
        if($params)
            ksort($params);
        $res = $this->client->post($uri, [
            'headers'=>[
                'Api-Key'=>$this->key,
                'Sign'=>$this->sign($params),
                'Accept'=>'application/json'
            ],
            'form_params'=>$params
        ]);
        return json_decode($res->getBody()->getContents(), true);
    }

    private function get($uri, $params = [])
    {
        if($params)
            ksort($params);
        $res = $this->client->get($uri, [
            'headers'=>[
                'Api-Key'=>$this->key,
                'Sign'=>$this->sign($params),
                'Accept'=>'application/json'
            ],
            'query'=>http_build_query($params)
        ]);
        return json_decode($res->getBody()->getContents(), true);
    }

    private function sign($params){
        $query = http_build_query($params);
        return strtoupper(hash_hmac('SHA256', $query, $this->secret));
    }

    public function getBalances()
    {
        if(($state = $this->getAccountState()) && $state->status === 'authorised'){
            $this->getIndexPage();
            $this->saveState();
        }
        $info = $this->get('payment/balances');
        $balances = [];
        foreach ($info as $balance){
            if($balance['type'] === 'available')
                array_push($balances, [
                    'name'=>$balance['currency'],
                    'balance'=>$balance['value']
                ]);
        }
        return $balances;
    }

    public function getRates()
    {
        $res = $this->client->get('exchange/ticker');
        $res = json_decode($res->getBody()->getContents(), true);
        $rates = [];
        foreach($res as $pair){
            $name = explode("/", $pair['symbol']);
            if(($name[1] === 'BTC' || $name[1] === 'USD') && floatval($pair['last']) > 0){
                if(!($presist = @$rates[$name[0]]) || $presist['to'] !== 'BTC'){
                    $rates[$name[0]] = [
                        'to'=>$name[1],
                        'rate'=>floatval($pair['last'])
                    ];
                }
            } elseif(($name[0] === 'USD' || $name[1] === 'RUR') && floatval($pair['last']) > 0) {
                $rates[$name[1]] = [
                    'to'=>$name[0],
                    'rate'=>1/floatval($pair['last'])
                ];
            }
        }
        return $rates;
    }

    public function getTransactionHistory(\Closure $callback)
    {
        $now = Carbon::now();
        $till = ($last = $this->account->transactions()->latest('id')->first()) ? $last->date : $this->account->created_at->timestamp * 1000;
        $res = $this->get('payment/history/transactions',['end'=>$now->timestamp * 1000, 'start'=>$till]);
        foreach($res as $tr){
            $deb = true;
            $type = strtolower($tr['type']);
            if($type === 'deposit' || $type === 'buy')
                $deb = false;
            $name = isset($this->reverse_currencies[$tr['fixedCurrency']]) ? $this->reverse_currencies[$tr['fixedCurrency']] : $tr['fixedCurrency'];

            if(!$callback([
                'value'=>$tr['amount'] - $tr['fee'],
                'currency'=>$name,
                'debit'=>$deb,
                'info'=>$tr['externalKey'],
                'date'=>$tr['date']
            ])){
                break;
            }
        }
    }

    private function getCookies(){
        return $this->client->getConfig('cookies');
    }

    public function saveState()
    {
        $cookies_array = $this->getCookies();
        $carr = [];

        foreach($cookies_array as $cookie){
            array_push($carr, [
                'name'     => $cookie->getName(),
                'value'    => $cookie->getValue(),
                'domain'   => $cookie->getDomain(),
                'path'     => $cookie->getPath(),
                'expires'  => $cookie->getExpires()
            ]);
        }
        $cookie_setting = $this->account->settings()->where('name', 'cookies')->first();
        if($cookie_setting){
            $this->account->settings()->updateExistingPivot($cookie_setting->id, [
                'value'=>json_encode($carr)
            ]);
        }
        else{
            $this->account->settings()->attach(AccountSetting::where('name', 'cookies')->first()->id, [
                'value'=>json_encode($carr)
            ]);
        }
    }

    /**
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws ApiErrorException
     * @throws ManualAccountConfirmationNeededException
     * @throws WrongCaptchaException
     */
    public function topUp()
    {
        $auth = $this->Auth();
        if($auth === true){
            Log::info("authorised", ['account_id'=>$this->account->id]);
            $this->setAccountState([
                'status'=>'authorised'
            ]);
            $this->saveState();
            $filtered = $this->validate(request(), [
                'currency'=>'required',
                'amount'=>'required',
                'phone'=>'required',
                'receive_amount'=>'required'
            ]);
            $form = [
                'CashInPiastrixForm[currency]'=>strtoupper($filtered['currency']),
                'CashInPiastrixForm[amount]'=>$filtered['amount'],
                'CashInPiastrixForm[phone]'=>$filtered['phone'],
                'CashInPiastrixForm[receiveAmount]'=>($filtered['receive_amount']),
                'CashInPiastrixForm[isAgree]'=>1,
                'yt0'=>'Оплатить'
            ];
            foreach($this->getCashInPiastrix($filtered['currency']) as $key => $value)
                $form[$key] = $value;
            $res = $this->client->post('https://www.livecoin.net/ru/cashInPiastrix/payment', [
                'form_params'=>$form
            ])->getBody()->getContents();
            $form = [];
            if(preg_match_all("/<form method=\"GET\" action=\"https\:\/\/qiwi\.com[^\"]+\"[^>]+>(.|\s)*?<\/form>/", $res, $form)){
                $this->phone = $filtered['phone'];
                Log::info("created payment form", ['account_id'=>$this->account->id]);
                $form = $form[0][0];
                $parsed_inputs = [];
                if(preg_match_all("/(<input.*?\/>)/", $form, $parsed_inputs)){
                    $form = "<form method=\"GET\" action=\"https://qiwi.com/payment/form.action\" target='_blank'>{{fields}}</form>";
                    $inputs = "";
                    foreach($parsed_inputs[0] as $input){
                        $inputs .= $input;
                    }
                    $inputs .= "{{submit}}";

                    return str_ireplace('{{fields}}', $inputs, $form);
                }
            }
            Log::info("cant create payment form", ['account_id'=>$this->account->id]);
        }
        return false;
    }

    private function getCashInPiastrix($currency = 'rur'){
        try{
            $res = $this->client->get('https://www.livecoin.net/ru/cashInPiastrix/index?currency='.strtoupper($currency))
                ->getBody()->getContents();
        } catch(ClientException $exception){
            Log::debug("cant create form", [
                "response"=>$exception->getResponse()->getBody()->getContents(),
                "request"=>$exception->getRequest()->getBody()->getContents()
            ]);
        }
        $form = [];
        $result = [];
        if(preg_match_all("/value=\"([^\"]+)\".*?name=\"(VFORMID|YII_CSRF_TOKEN)\"/", $res, $form)){
            for($i = 0; $i < count($form[0]); $i++){
                $result[$form[2][$i]] = $form[1][$i];
            }
        }
        return $result;
    }

    private function getIndexPage(){
        $url = 'https://www.livecoin.net/ru/finance/index';
        $res = $this->client->get($url, [
            'on_stats' => function (TransferStats $stats) use (&$url) {
                $url = $stats->getEffectiveUri();
            }
        ]);

        return is_string($url) || strpos($url->getPath(), 'login') === FALSE;
    }

    private function getLoginPage(&$csrf, &$captcha){
        $res = $this->client->get("https://www.livecoin.net/ru/site/login");
        $csrf_cookie = $this->client->getConfig('cookies')->getCookieByName('YII_CSRF_TOKEN');
        if(!$csrf_cookie)
            Log::info("created parse csrf-token", ['account_id'=>$this->account->id]);
        $csrf = explode('%22', $csrf_cookie->getValue())[1];
        $res = $res->getBody()->getContents();
        $matches = [];
        preg_match_all("/(\/[a-z]{2}\/site\/captcha\?v=[^\"]+)\"/", $res, $matches);
        if(!count($matches[0]))
            Log::info("created parse captcha code", ['account_id'=>$this->account->id, 'response'=>$res]);
        $captcha = $matches[1][0];
    }

    private function GetSessionSecurityAttributes($captcha, $csrf){
        $response = $this->client->post('https://www.livecoin.net/site/login',[
            'headers'=>[
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'form_params'=>[
                'ajaxMethod'=>'GetSessionSecurityAttributes',
                'captchaCode'=>$captcha,
                'YII_CSRF_TOKEN'=>$csrf
            ]
        ])->getBody()->getContents();
        $res = json_decode($response, true);
        return $res;
    }

    private function LoginByPassword($csrf, $captcha, $username, $password){
        $rnd = str_random(32);
        $stat = "1366|768|24|24|{$this->ua}|true|en-US|Win32||none|{$rnd}";
        $response = $this->client->post('https://www.livecoin.net/site/login',[
            'headers'=>[
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'form_params'=>[
                'ajaxMethod'=>'LoginByPassword',
                'captchaCode'=>$captcha,
                'YII_CSRF_TOKEN'=>$csrf,
                'rememberMe'=>'1',
                'username'=>$username,
                'password'=>$password,
                'stat'=>$stat
            ]
        ])->getBody()->getContents();
        return json_decode($response, true);
    }

    private static function hash($password, $modulus, $e){
        $rsa = new RSA();
        $kr = $rsa->loadKey([
            'e'=> new BigInteger($e, 16),
            'n'=> new BigInteger($modulus, 16)
        ]);
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        return bin2hex($rsa->encrypt($password));
    }

    private function solveCaptcha($code, &$task_id){
        $file = tmpfile();
        $metaDatas = stream_get_meta_data($file);
        $stream = stream_for($file);
        $this->client->get('https://www.livecoin.net'.$code,[
            'save_to'=>$stream
        ]);
        $api = $this->getCaptchaApi();
        $filename = $metaDatas['uri'];
        $api->setImageBody(base64_encode(file_get_contents($filename)));
        fclose($file);
        if(!$api->createTask())
            throw new ApiErrorException("cant_create_task");
        $task_id = $api->getTaskId();
        return null;
    }

    private function confirmEmail($code, $csrf){
        $response = $this->client->post('https://www.livecoin.net/site/emailConfirmIp',[
            'headers'=>[
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'form_params'=>[
                'code'=>$code,
                'YII_CSRF_TOKEN'=>$csrf
            ]
        ])->getBody()->getContents();
        return strpos($response, 'emailConfirmIpContainer') === FALSE;
    }

    private function dropState(){
        if(($state = AccountSetting::where('name', 'state')->first())) {
            $this->account->settings()->detach($state->id);
        }
        if($cookie_setting = $this->account->settings()->where('name', 'cookies')->first()){
            $this->account->settings()->detach($cookie_setting->id);
        }
    }



    /**
     * @return bool
     * @throws ApiErrorException
     * @throws ManualAccountConfirmationNeededException
     * @throws WrongCaptchaException
     */
    public function Auth()
    {
        $state = $this->getAccountState();
        if($state){
            if($state->status === 'mail_confirmation') {
                $code = request()->get('code');
                if (!$code)
                    throw new ManualAccountConfirmationNeededException();
                if (!$this->confirmEmail($code, $state->csrf_token)){
                    if(time() > $state->until){
                        $this->dropState();
                        return false;
                    }
                    throw new ManualAccountConfirmationNeededException();
                }else{
                    $this->setAccountState([
                        'status'=>'authorised',
                        'until'=>time()+(60*60*2),
                        'csrf_token'=>$state->csrf_token
                    ]);
                    $this->saveState();
                    return true;
                }
            } elseif($state->status === 'captcha') {
                $api = $this->getCaptchaApi();
                $api->setTaskId($state->task_id);
                if($api->waitForResult()){
                    $captcha = $api->getTaskSolution();
                    try{
                        $res = $this->continueAuth($captcha, $state->csrf_token);
                        return $res;
                    }catch (WrongCaptchaException $e){
                        $this->dropState();
                        return false;
                    }
                } else {
                    if($api->getErrorMessage())
                        $this->dropState();
                    return false;
                }
            } elseif($state->status === 'authorised') {
                $auth = $this->getIndexPage();
                if(!$auth)
                    $this->dropState();
                else {
                    $this->saveState();
                }
                return $auth;
            }
        } else {
            $csrf = '';
            $captchaUrl = '';
            $this->getLoginPage($csrf, $captchaUrl);
            $task_id = "";
            $captcha = $this->solveCaptcha($captchaUrl, $task_id);
            if(!$captcha){
                $this->setAccountState([
                    'status'=>'captcha',
                    'until'=>time()+(60*15),
                    'csrf_token'=>$csrf,
                    'task_id'=>$task_id
                ]);
                $this->saveState();
                return false;
            } else {
                return $this->continueAuth($captcha, $csrf);
            }
        }
        return false;
    }

    /**
     * @param $captcha
     * @param $csrf
     * @return bool
     * @throws ManualAccountConfirmationNeededException
     * @throws WrongCaptchaException
     */
    private function continueAuth($captcha, $csrf){
        $sec = $this->GetSessionSecurityAttributes($captcha, $csrf);
        if(!$sec){
            $this->setAccountState([
                'status'=>'mail_confirmation',
                'until'=>time()+(60*60),
                'csrf_token'=>$csrf
            ]);
            $this->saveState();
            throw new ManualAccountConfirmationNeededException("mail_confirmation");
        }
        if($sec['success']){
            $password = static::hash($this->password, $sec['modulus'], $sec['exponent']);
            $res = $this->LoginByPassword($csrf, $captcha, $this->email, $password);
            if($res['success']){
                if(!$res['logged']){
                    $this->account->status()->associate(AccountStatus::where('name', 'error')->first());
                    $this->account->description = "Cant auth with this credentials";
                    $this->account->save();
                    return false;
                }

                if(isset($res['needIpConfirm']) && $res['needIpConfirm']){
                    $this->setAccountState([
                        'status'=>'mail_confirmation',
                        'until'=>time()+(60*60),
                        'csrf_token'=>$csrf
                    ]);
                    $this->confirmEmail('', $csrf);
                    $this->saveState();
                    throw new ManualAccountConfirmationNeededException("mail_confirmation");
                }
                $this->setAccountState([
                    'status'=>'authorised'
                ]);
                $this->saveState();
                return true;
            } else {
                return false;
            }
        } else {
            throw new WrongCaptchaException();
        }
    }

    private function stateResponse($state, $params = null){
        $p = [
            'state'=>$state
        ];
        if($params){
            array_merge($p, $params);
        }
        return view('layouts.proceeding', $p);
    }

    private function getCaptchaApi(){
        $api = new ImgToTextCaptcha();
        $setting = SiteSetting::query()->where('name', 'anticaptcha_key')->first();
        if($setting)
            $api->setKey($setting->value);
        else
            $api->setKey(config('anticaptcha.key'));
        return $api;
    }

    public function CheckTransactionCompletion($currency, $amount, $info, $from)
    {
        return AccountTransaction::query()->where('currency', $currency)
                ->whereBetween('value', [$amount - 1, $amount + 1])
                ->where('date', '>=', $from)
                ->whereNull('used_at')
                ->where('info', 'like', "%{$info}%")->first();
    }

    public function GetTransactionDetails()
    {
        return $this->phone;
    }

    /**
     * @param string $currency
     * @param float $amount
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws ApiErrorException
     * @throws ManualAccountConfirmationNeededException
     * @throws WrongCaptchaException
     */
    public function GetPaymentLink($currency, $amount)
    {
        request()->merge([
            'currency'=>$currency,
            'amount'=>$amount
        ]);
        return $this->topUp();
    }

    private static $methods = [
        'qiwi'
    ];

    public function GetMethodIdByName($name)
    {
        return array_flip(static::$methods)[$name];
    }

    public function GetButton($src)
    {
        $subbut = "<button class=\"button is-info is-large is-inverted\" type=\"submit\">
                            <span class=\"icon\">
                              <i class=\"fa fa-credit-card\"></i>
                            </span>
                    <span>{{title}}</span>
                </button>";
        $subbut = str_ireplace("{{title}}", trans('pages.checkout.proceed'), $subbut);
        return str_ireplace("{{submit}}", $subbut, str_ireplace("type=\"text\"", "type=\"hidden\"", str_ireplace("type='text'", "type=\"hidden\"", $src)));
    }
}