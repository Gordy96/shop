<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 04/06/2018
 * Time: 09:00
 */

namespace App\Helpers\Payments\Platforms;


use App\Account;
use App\AccountSetting;
use App\AccountTransaction;
use App\Helpers\Exceptions\BadAuthException;
use App\Helpers\Payments\Platforms\StockApiInterface;
use App\Helpers\PaymentSystemInterface;
use App\Helpers\RandomUserAgent;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Psr\Http\Message\ResponseInterface;

class WexNzApi implements StockApiInterface, PaymentSystemInterface
{
    use ValidatesRequests;
    /**
     * @var Client
     */
    private $client = null;

    /**
     * @var \GuzzleHttp\Cookie\CookieJar
     */
    public $response_cookies = null;

    const base = 'https://wex.nz/';

    /**
     * @var string
     */
    private $email = null;

    /**
     * @var string
     */
    private $password = null;

    /**
     * @var string
     */
    private $target = null;

    /**
     * @var string
     */
    private $data = null;

    /**
     * @var bool
     */

    private $logged_in = false;

    private $final_data = null;
    private $final_csrf = null;

    private $prof_html = null;
    private $index_html = null;

    private $account = null;

    private $transaction_info = null;

    /**
     * Api constructor.
     * @param mixed $account
     */

    public function __construct($account)
    {
        $defs = [
            'base_uri'=>static::base,
            'verify'=>false,
            'cookies'=>true,
            'headers'=>[
            ]
        ];

        if($account instanceof Account){
            $this->account = $account;

            $ua = $account->settings()->where('name', 'user-agent')->first();
            if(!$ua){
                $ua = RandomUserAgent::getRandomUserAgent();
                $setting = AccountSetting::where('name', 'user-agent')->first();
                $account->settings()->attach($setting->id, [
                    'value'=>$ua
                ]);
            } else {
                $ua = $ua->info->value;
            }

            $defs['headers']['User-Agent'] = $ua;

            $creds = $account->settings()->where('name', 'credentials')->first()->info->value;
            $username = $creds->username;
            $password = $creds->password;
            $proxy = $account->settings()->where('name', 'proxy')->first();
            $this->email = $username;
            $this->password = $password;

            if($proxy)
                $defs['proxy'] = $proxy->info->value;

            if(($cookies = $account->settings()->where('name', 'cookies')->first()) && $cookies->info->value) {
                $this->setCookies($cookies->info->value);
                $defs['cookies'] = $this->response_cookies;
            }
        } elseif(is_string($account)) {
            $defs['proxy'] = $account;
        }

        $this->client = new Client($defs);
    }

    private function check(){
        $this->response_cookies = $this->client->getConfig('cookies');
    }

    /**
     * @return $this
     */

    public function index(){
        $res = $this->client->get('/');
        $this->index_html = $res->getBody()->getContents();
        $this->check();
        return $this;
    }

    /**
     * @return $this
     * @throws BadAuthException
     */

    public function getToken(){
        $jar = $this->response_cookies;
        $res = $this->client->get('/', [
            'headers'=>[
                'Referer'=>static::base,
                'Origin'=>substr(static::base, 0, -1),
            ],
            'cookies' => $jar
        ]);
        $response = $res->getBody()->getContents();
        $csrf = [];
        preg_match_all('/"csrf-token" *value="([^"]+)"/', $response, $csrf);
        if(!count($csrf[1]))
            throw new BadAuthException($this->email, $this->password);
        $this->final_csrf = $csrf[1][0];
        preg_match_all('/"funds", *([0-9]+)/', $response, $csrf);
        $this->final_data = $csrf[1][0];
        $this->check();
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */

    public function getCaptcha(){
        $this->client->get('/getcaptcha', [
            'headers'=>[
                'Referer'=>static::base
            ]
        ]);
        $this->check();
        return $this;
    }

    /**
     * @param string $pow_nonce
     * @throws BadAuthException
     * @throws \BadMethodCallException
     * @return $this
     */

    public function login($pow_nonce = ''){
        $jar = $this->response_cookies;
        $jar->setCookie(new \GuzzleHttp\Cookie\SetCookie([
            'Name'     => 'chatRefresh',
            'Value'    => 0,
            'Domain'   => static::base,
            'Path'     => '/'
        ]));
        $res = $this->client->post('/ajax/login', [
            'headers'=>[
                'Accept' => '*/*',
                'Accept-Language' => 'en-US,en;q=0.8,hi;q=0.6,und;q=0.4',
                'Referer'=>static::base,
                'Origin'=>substr(static::base, 0, -1),
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'cookies' => $jar,
            'form_params'=>[
                'csrfToken'=>'',
                'email'=>$this->email,
                'password'=>$this->password,
                'otp'=>'-',
                'PoW_nonce'=>$pow_nonce,
                'captcha_input'=>''
            ]
        ]);
        $res = \GuzzleHttp\json_decode($res->getBody()->getContents());
        if(!$res->success)
            throw new BadAuthException($this->email, $this->password);

        if($res->success && $pow_nonce){
            if($res->data->login_success)
                $this->logged_in = true;
            else
                throw new BadAuthException($this->email, $this->password, 'probably wrong pow');
        } else {
            $this->data = $res->data->work->data;
            $this->target = $res->data->work->target;
        }
        $this->check();

        return $this;
    }

    /**
     * @throws \Exception
     */

    private function profile(){
        if(!$this->final_data || !$this->final_csrf)
            throw new \BadMethodCallException('data and csrf-token must be initialized at this point');
        $jar = $this->response_cookies;
        $res = $this->client->post('/ajax/profile', [
            'headers'=>[
                'Accept' => '*/*',
                'Accept-Language' => 'en-US,en;q=0.8,hi;q=0.6,und;q=0.4',
                'Referer'=>static::base.'profile',
                'Origin'=>substr(static::base, 0, -1),
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'cookies' => $jar,
            'form_params'=>[
                'csrfToken'=>$this->final_csrf,
                'task'=>'funds',
                'data'=>$this->final_data
            ]
        ]);
        $this->check();
        $res = $res->getBody()->getContents();
        if($res === "must be logged")
            throw new BadAuthException($this->email, $this->password);
        $this->prof_html = $res;
    }

    /**
     * @param  string $data
     * @param  int $page
     * @throws \Exception
     * @return string
     */

    private function transactions($data, $page = 1){
        $jar = $this->response_cookies;
        $res = $this->client->post('/ajax/billing', [
            'headers'=>[
                'Accept' => '*/*',
                'Accept-Language' => 'en-US,en;q=0.8,hi;q=0.6,und;q=0.4',
                'Referer'=>static::base.'profile',
                'Origin'=>substr(static::base, 0, -1),
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'cookies' => $jar,
            'form_params'=>[
                'csrfToken'=>$this->final_csrf,
                'act'=>'history',
                'id'=>$data,
                'page'=>$page
            ]
        ]);
        $this->check();
        $res = $res->getBody()->getContents();
        if($res === "must be logged")
            throw new BadAuthException($this->email, $this->password);
        return $res;
    }


    /**
     * @return array
     * @throws \Exception
     */
    private function parse_profile_html(){
        preg_match_all("/<h3 style='margin-bottom: 2px;'>([A-Za-z0-9]+)<\/h3>([\r\n].*){1,}<b>([A-Za-z0-9.,]+)<\/b> *\b\\1\b.*<b>([A-Za-z0-9.,]+)/", $this->prof_html, $entries);

        if($entries){
            $result = [];
            for($i = 0; $i < count($entries[0]); $i++)
                array_push($result, [
                    'name'=>$entries[1][$i],
                    'balance'=>$entries[3][$i],
                    'tokens'=>$entries[4][$i],
                ]);
            return $result;
        }
        throw new \Exception("request malformed or regular expression failed");
    }

    private function mix_md5($md5_str)
    {
        if (strlen($md5_str) == 32)
            return substr($md5_str, 16, 8) . substr($md5_str, 0, 8) . substr($md5_str, 8, 8) . substr($md5_str, 24, 8);
        else
            throw new \BadMethodCallException('invalid md5');
    }

    private function reverse_md5($source)
    {
        return strrev(md5($source));
    }

    private function get_proof_of_work($target, $data)
    {
        $c = 0;
        do
        {
            $temp = "0x" . $this->mix_md5($this->reverse_md5($this->reverse_md5($data . $c)));
            $hash = (float) gmp_strval(gmp_init($temp));
            $c++;
        }
        while ($hash >= $target);

        return $c;
    }

    public function solvePoW(){
        if(!$this->target || !$this->data)
            throw new \BadMethodCallException('data and target must be initialized at this point');
        $target = $this->target;
        $data = $this->data;
        $this->target = null;
        $this->data = null;
        return $this->get_proof_of_work($target, $data);
    }

    public function getBalances()
    {
        $this->Auth();
        $this->saveState();
        $res = $this->parse_profile_html();
        return $res;
    }

    public function topUp()
    {
        $filtered = $this->validate(request(), [
            'currency'=>'required',
            'amount'=>'required'
        ]);
        if (!$this->prof_html) {
            if (!$this->response_cookies) {
                $this->index()->getCaptcha()->login()->login($this->solvePoW())->getToken()->profile();
            } else {
                try{
                    $this->getToken()->profile();
                } catch (BadAuthException $e){
                    $this->index()->getCaptcha()->login()->login($this->solvePoW())->getToken()->profile();
                }
            }
        }
        $currency = $filtered['currency'];
        $amount = $filtered['amount'];
        $jar = $this->response_cookies;
        $res = $this->client->post('/ajax/billing',[
            'headers'=>[
                'Accept' => '*/*',
                'Accept-Language' => 'en-US,en;q=0.8,hi;q=0.6,und;q=0.4',
                'Referer'=>static::base.'profile',
                'Origin'=>substr(static::base, 0, -1),
                'X-Requested-With'=>'XMLHttpRequest'
            ],
            'cookies' => $jar,
            'form_params'=>[
                'csrfToken'=>$this->final_csrf,
                'act'=>'deposit/' . strtolower($currency),
            ]
        ]);
        $res = $res->getBody()->getContents();
        $entries = [];
        if(preg_match_all("/(<form.*action=\"https\:\/\/money.yandex.ru[^>]+>(.|\s)*?<\/form>)/", $res, $entries)){
            preg_match_all("/name=\"([^\"]+)\".*value=\"([^\"]+)\"/", $entries[1][0], $entries);
            $form = "<form method=\"POST\" action=\"https://money.yandex.ru/quickpay/confirm.xml\">{{form}}</form>";
            $fields = "";
            for($i = 0; $i < count($entries[0]); $i++){
                if($entries[1][$i] === 'sum')
                    $fields .= "<input type='hidden' name='{$entries[1][$i]}' value='{$amount}'/>";
                else{
                    if($entries[1][$i] === 'label')
                        $this->SetTransactionDetails(["id"=>$entries[2][$i]]);
                    $fields .= "<input type='hidden' name='{$entries[1][$i]}' value='{$entries[2][$i]}'/>";
                }
            }
            $fields .= "<input type='submit' value='Continue'/>";
            $this->saveState();
            return view("layouts.autoform",['form'=>str_ireplace('{{form}}', $fields, $form)]);
        }
        throw new \ErrorException("cant parse form");
    }

    public function getCookies()
    {
        $this->check();
        return $this->response_cookies;
    }

    public function getTransactionHistory(\Closure $callback)
    {
        $output_array = [];
        if(preg_match_all("/trans_history\(([0-9]+)/", $this->prof_html, $output_array) && count($output_array[1])){
            $data = $output_array[1][0];
            $page = 1;
            while(true){
                $tr_ = $this->transactions($data, $page);
                if(preg_match_all("/#([^<]+)<(\s|.)+?((\+|-)[0-9]+.[0-9]+) *.*>([A-Za-z0-9]+).*([\r\n]+.*){1,2}>([0-9.]+).*>([0-9:]+)/", $tr_, $output_array)){
                    for($i = 0; $i < count($output_array[0]); $i++){
                        $date = Carbon::createFromFormat('d.m.y H:i:s',$output_array[7][$i] . ' ' . $output_array[8][$i])->timestamp * 1000;
                        $deb = $output_array[4][$i] === '-' ? true : false;
                        $value = floatval(substr($output_array[3][$i],1));
                        if(!$callback([
                            'value'=>$value,
                            'debit'=>$deb,
                            'info'=>["id"=>$output_array[1][$i]],
                            'date'=>$date
                        ]))
                            break;
                    }
                } else {
                    break;
                }
                $page++;
            }
        }
    }

    public function setCookies($cookies)
    {
        $this->response_cookies = new CookieJar();
        foreach ($cookies as $cookie)
            $this->response_cookies->setCookie(
                new SetCookie([
                    'Name'     => $cookie->name,
                    'Value'    => $cookie->value,
                    'Domain'   => $cookie->domain,
                    'Path'     => $cookie->path,
                    'Expires'  => $cookie->expires
                ])
            );
    }

    public function getRates()
    {
        if(!$this->index_html){
            $this->index();
        }

        $res = [];
        $output_array = [];
        if(preg_match_all("/>([A-Z]{3,})\\/([A-Z]{3,})[^>]+>([0-9.]+)/", $this->index_html, $output_array)){
            for($i = 0; $i < count($output_array[0]); $i++){
                if($output_array[2][$i] === 'BTC' || $output_array[2][$i] === 'USD'){
                    if(!($presist = @$res[$output_array[1][$i]]) || $presist['to'] !== 'BTC'){
                        $res[$output_array[1][$i]] = [
                            'to'=>$output_array[2][$i],
                            'rate'=>floatval($output_array[3][$i])
                        ];
                    }
                } elseif ($output_array[1][$i] === 'USD' && $output_array[2][$i] === 'RUR') {
                    $res['RUR'] = [
                        'to'=>'USD',
                        'rate'=>1.0/floatval($output_array[3][$i])
                    ];
                }
            }
        }
        return $res;
    }

    public function saveState()
    {
        $cookies_array = $this->getCookies();
        $carr = [];

        foreach($cookies_array as $cookie){
            array_push($carr, [
                'name'     => $cookie->getName(),
                'value'    => $cookie->getValue(),
                'domain'   => $cookie->getDomain(),
                'path'     => $cookie->getPath(),
                'expires'  => $cookie->getExpires()
            ]);
        }
        $cookie_setting = $this->account->settings()->where('name', 'cookies')->first();
        if($cookie_setting){
            $this->account->settings()->updateExistingPivot($cookie_setting->id, [
                'value'=>json_encode($carr)
            ]);
        }
        else{
            $this->account->settings()->attach(AccountSetting::where('name', 'cookies')->first()->id, [
                'value'=>json_encode($carr)
            ]);
        }
    }

    public function Auth()
    {
        if (!$this->prof_html) {
            if (!$this->response_cookies) {
                $this->index()->getCaptcha()->login()->login($this->solvePoW())->getToken()->profile();
            } else {
                try{
                    $this->getToken()->profile();
                } catch (BadAuthException $e){
                    $this->index()->getCaptcha()->login()->login($this->solvePoW())->getToken()->profile();
                }
            }
        } else {
            try{
                $this->getToken()->profile();
            } catch (BadAuthException $e){
                $this->index()->getCaptcha()->login()->login($this->solvePoW())->getToken()->profile();
            }
        }
        $this->saveState();
    }

    public function SetTransactionDetails($details)
    {
        $this->transaction_info = $details;
    }

    public function GetTransactionDetails()
    {
        return $this->transaction_info;
    }

    public function CheckTransactionCompletion()
    {
        // TODO: Implement CheckTransactionCompletion() method.
    }

    public function GetPaymentLink($currency, $amount)
    {
        request()->merge([
            'currency'=>$currency,
            'amount'=>$amount
        ]);
        return $this->topUp();
    }
}