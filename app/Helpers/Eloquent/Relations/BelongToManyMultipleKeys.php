<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 27/06/2018
 * Time: 19:37
 */

namespace App\Helpers\Relations;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class BelongToManyMultipleKeys extends BelongsToMany
{
    /**
     * @var \Closure
     */
    private $callback;

    /**
     * BelongToManyMultipleKeys constructor.
     * @param Builder $query
     * @param Model $parent
     * @param string $table
     * @param string $foreignPivotKey
     * @param string $relatedPivotKey
     * @param string $parentKey
     * @param string $relatedKey
     * @param null|string $relationName
     * @param \Closure|null $cb
     */
    public function __construct(Builder $query, Model $parent, string $table, string $foreignPivotKey, string $relatedPivotKey, string $parentKey, string $relatedKey, ?string $relationName = null, ?\Closure $cb = null)
    {
        $this->callback = $cb;
        parent::__construct($query, $parent, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relationName);
    }

    /**
     * Set the where clause for the relation query.
     *
     * @return $this
     */
    protected function addWhereConstraints()
    {
        if($this->callback){
            $cb = $this->callback;
            $keys = collect($cb())->unique()->sort()->all();
            $this->query->where(
                $this->getQualifiedForeignPivotKeyName(), "=", $this->parent->{$this->parentKey}
            );
        } else {
            $this->query->where(
                $this->getQualifiedForeignPivotKeyName(), '=', $this->parent->{$this->parentKey}
            );
        }

        return $this;
    }
}