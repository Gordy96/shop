<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 27/06/2018
 * Time: 22:55
 */

namespace App\Helpers\Eloquent\Relations;


use App\Helpers\Eloquent\Builder;

class BelongsToMany extends \Illuminate\Database\Eloquent\Relations\BelongsToMany
{

    /**
     * The Eloquent query builder instance.
     *
     * @var Builder
     */
    protected $query;

    /**
     * Handle dynamic method calls to the relationship.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (static::hasMacro($method)) {
            return $this->macroCall($method, $parameters);
        }

        $result = $this->query->{$method}(...$parameters);

        if ($result === $this->query) {
            return $this;
        }

        return $result;
    }

    /**
     * Specify the custom pivot accessor to use for the relationship.
     *
     * @param  string  $accessor
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function as($accessor)
    {
        $this->accessor = $accessor;
        $this->setPivotAccessor($this->accessor);
        return $this;
    }
}