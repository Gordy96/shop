<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 27/06/2018
 * Time: 18:44
 */

namespace App\Helpers\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class HasManyMultipleLocalKeys extends HasMany
{
    /**
     * @var \Closure
     */
    private $callback;

    /**
     * HasManyMultipleLocalKeys constructor.
     * @param Builder $query
     * @param Model $parent
     * @param string $foreignKey
     * @param string $localKey
     * @param \Closure|null $additionalKeys
     */
    public function __construct(Builder $query, Model $parent, string $foreignKey, string $localKey, \Closure $additionalKeys = null)
    {
        $this->callback = $additionalKeys;
        parent::__construct($query, $parent, $foreignKey, $localKey);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        if (static::$constraints) {
            if($this->callback){
                $cb = $this->callback;
                $keys = collect($cb())->push($this->getParentKey())->unique()->sort()->all();
                $this->query->whereIn($this->foreignKey, $keys);
            } else {
                $this->query->where($this->foreignKey, '=', $this->getParentKey());
            }

            $this->query->whereNotNull($this->foreignKey);
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        $this->query->whereIn(
            $this->foreignKey, $this->getKeys($models, $this->localKey)
        );
    }

    /**
     * Get all of the primary keys for an array of models.
     *
     * @param  array   $models
     * @param  string  $key
     * @return array
     */
    protected function getKeys(array $models, $key = null)
    {
        $c = collect($models)->map(function ($value) use ($key) {
            return $key ? $value->getAttribute($key) : $value->getKey();
        })->values()->merge($this->callback());

        return $c->unique()->sort()->all();
    }
}