<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/06/2018
 * Time: 10:12
 */

namespace App\Helpers\Eloquent;


use App\Helpers\Eloquent\Relations\BelongsToMany;

class Model extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Override. Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    /**
     * Instantiate a new BelongsToMany relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $table
     * @param  string  $foreignPivotKey
     * @param  string  $relatedPivotKey
     * @param  string  $parentKey
     * @param  string  $relatedKey
     * @param  string  $relationName
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function newBelongsToMany(\Illuminate\Database\Eloquent\Builder $query,
                                        \Illuminate\Database\Eloquent\Model $parent, $table,
                                        $foreignPivotKey, $relatedPivotKey,
                                        $parentKey, $relatedKey, $relationName = null)
    {
        return new BelongsToMany($query, $parent, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relationName);
    }

    public function generateSlug(){
        return null;
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function(Model $model){
            if($model->isFillable('slug') && !$model->slug){
                $model->slug = $model->generateSlug();
            }
        });

//        self::created(function($model){
//            // ... code here
//        });
//
//        self::updating(function($model){
//            // ... code here
//        });
//
//        self::updated(function($model){
//            // ... code here
//        });
//
//        self::deleting(function($model){
//            // ... code here
//        });
//
//        self::deleted(function($model){
//            // ... code here
//        });
    }
}