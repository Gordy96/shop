<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 27/06/2018
 * Time: 22:39
 */

namespace App\Helpers\Eloquent;


use Closure;

class Builder extends \Illuminate\Database\Eloquent\Builder
{

    private $accessors = ['pivot'];
    private $current_accessor = 'pivot';

    public function setPivotAccessor($accessor){
        array_push($this->accessors, $accessor);
    }

    public function getPivotAccessor(){
        return $this->current_accessor;
    }

    /**
     * Eager load pivot relations.
     *
     * @param  array $models
     * @return void
     */
    protected function loadPivotRelations($models)
    {
//        $query = head($models)->getRelation($this->getPivotAccessor())->newQuery();
        $pivots = collect($models)->map(function($model){
            return $model->getRelation($this->getPivotAccessor());
        })->all();
        $pivots = head($pivots)->newCollection($pivots);

        $pivots->load($this->getPivotRelations());
    }

    /**
     * Get the pivot relations to be eager loaded.
     *
     * @return array
     */
    protected function getPivotRelations()
    {
        $relations = array_filter(array_keys($this->eagerLoad), function ($relation) {
            return $relation != $this->getPivotAccessor() && str_contains($relation, $this->getPivotAccessor());
        });

        return array_map(function ($relation) {
            return substr($relation, strlen($this->getPivotAccessor() . '.'));
        }, $relations);
    }

    protected function eagerLoadRelation(array $models, $name, Closure $constraints)
    {
        if(in_array($name, $this->accessors)){
            $this->current_accessor = $name;
            $this->loadPivotRelations($models);
            return $models;
        }
        return parent::eagerLoadRelation($models, $name, $constraints);
    }
}