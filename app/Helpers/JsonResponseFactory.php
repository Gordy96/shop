<?php
/**
 * Created by PhpStorm.
 * User: Johnny
 * Date: 28/06/2018
 * Time: 12:36
 */

namespace App\Helpers;


class JsonResponseFactory
{
    public static function success($body){
        return response()->json([
            'success'=>true,
            'result'=>$body
        ]);
    }
    public static function fail($body){
        return response()->json([
            'success'=>false,
            'error'=>$body
        ]);
    }
}