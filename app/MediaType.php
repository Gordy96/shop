<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function media()
    {
        return $this->hasMany(Media::class, 'type_id');
    }
}
