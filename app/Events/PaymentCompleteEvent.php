<?php

namespace App\Events;

use App\AccountTransaction;
use App\Payment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PaymentCompleteEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $p;
    private $t;

    /**
     * Create a new event instance.
     * @param $payment Payment
     * @param $transaction AccountTransaction
     * @return void
     */
    public function __construct(Payment $payment, AccountTransaction $transaction)
    {
        $this->p = $payment;
        $this->t = $transaction;
    }

    /**
     * @return Payment
     */
    public function payment(){
        return $this->p;
    }

    /**
     * @return AccountTransaction
     */
    public function transaction(){
        return $this->t;
    }
}
