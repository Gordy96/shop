<?php

namespace App;

use App\Helpers\Eloquent\Model;
use App\Helpers\Payments\Platforms\StockApiInterface;
use App\Helpers\PaymentSystemInterface;

class PaymentMethod extends Model
{
    protected $fillable = [
        'name',
        'class',
        'info'
    ];

    protected $casts = [
        'info'=>'object'
    ];

    public $timestamps = false;

    public function payments(){
        return $this->hasMany(Payment::class, 'method_id');
    }

    public function accounts(){
        return $this->hasMany(Account::class, 'method_id');
    }

    /**
     * @param Account|null $account
     * @return PaymentSystemInterface|StockApiInterface
     */
    public function createApiInstance($account = null){
        $cn = $this->class;
        if(!$account)
            $account = $this->accounts()->inRandomOrder()->whereHas('status', function($builder) {
                $builder->where('name', 'active');
            })->first();
        return new $cn($account);
    }

    public function getMethods(){
        return $this->info;
    }
}