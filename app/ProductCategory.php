<?php

namespace App;

use App\Helpers\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = [
        'parent_id',
        'name'
    ];

    public $timestamps = false;

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    public function sub_categories()
    {
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function getConstructedName()
    {
        $name = [["name"=>$this->name, "id"=>$this->id]];
        if($this->parent){
            $name = array_merge($name, $this->parent->getConstructedName());
        }

        return $name;
    }

    public function getParentChain()
    {
        $temp = $this;
        $ancestors = [];
        array_push($ancestors, $temp);
        while(($parent = $temp->parent)){
            array_push($ancestors, $parent);
            $temp = $parent;
        }
        return array_reverse($ancestors);
    }
}
