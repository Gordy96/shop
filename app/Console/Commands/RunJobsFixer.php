<?php

namespace App\Console\Commands;

use App\Jobs\JobsFixer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RunJobsFixer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts:jobs_fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs a job which keeps an eye on every account`s jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jc = DB::table('jobs')
            ->where('queue', 'accounts.keeper')
            ->count();
        if(!$jc)
            dispatch(new JobsFixer())->onQueue('accounts.keeper');
    }
}
