<?php

namespace App\Console\Commands;

use App\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateDefaultAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:default_user 
                                    {email : authentication email}
                                    {--password=123123 : authentication password}
                                    {--roles=admin : one or more of (admin,content_manager,support) separated by comma}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $u = \App\User::create([
            'email'=>$this->argument('email'),
            'password'=>Hash::make($this->option('password'))
        ]);
        $roles = explode(',', $this->option('roles'));
        $u->roles()->sync(Role::query()->whereIn('name', $roles)->pluck('id'));
        return 0;
    }
}
