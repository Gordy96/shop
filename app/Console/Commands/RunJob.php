<?php

namespace App\Console\Commands;

use App\Jobs\AccountKeeper;
use App\Jobs\JobsFixer;
use App\Jobs\ReleaseOrderItems;
use App\Order;
use Illuminate\Console\Command;

class RunJob extends Command
{
    protected $jobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:job {which} {--model=} {--arg=} {--type=} {--delay=} {--queue=} {--connection=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jobName = $this->argument('which');

        if($this->option('model')){
            $modelName = $this->option('model');
            $arg = $modelName::find($this->option('arg'));
            $job = dispatch( new $jobName($arg));
        } else {
            if($this->option('arg') !== null){
                $arg = $this->option('arg');
                $job = dispatch( new $jobName($arg));
            } else {
                $job = dispatch( new $jobName());
            }
        }

        if($this->option('queue'))
            $job->onQueue($this->option('queue'));
        if($this->option('delay'))
            $job->delay($this->option('delay'));
        if($this->option('connection'))
            $job->onConnection($this->option('connection'));

        return 0;
    }
}
