<?php

namespace App;

use App\Helpers\Eloquent\Model;

class ItemStatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function items(){
        return $this->hasMany(Item::class, 'status_id');
    }
}
