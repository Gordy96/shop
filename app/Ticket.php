<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'category_id',
        'status_id',
        'owner_id',
        'assignee_id',
        'theme',
        'key',
        'contact'
    ];

    public function messages(){
        return $this->hasMany(TicketMessage::class, 'ticket_id');
    }

    public function owner(){
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function assignee(){
        return $this->belongsTo(User::class, 'assignee_id');
    }

    public function status(){
        return $this->belongsTo(TicketStatus::class, 'status_id');
    }

    public function category(){
        return $this->belongsTo(TicketCategory::class, 'category_id');
    }
}
