<?php

namespace App;

use App\Helpers\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id',
        'price',
        'name_ru',
        'name_en',
        'description',
        'rating',
        'published_at',
        'category_id'
    ];

    public $hidden = [
        'name_en', 'name_ru'
    ];

    protected $appends = [
        'title_image',
        'name'
    ];

    public function attributes(){
        return $this->belongsToMany(ProductAttribute::class, 'product_product_attribute', 'product_id', 'attribute_id')
            ->withPivot(['value'])
            ->as('data');
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function orders(){
        return $this->belongsToMany(Order::class, 'product_order', 'product_id', 'order_id')
            ->using(OrderProductItemPivot::class)
            ->withPivot(['product_id', 'item_id'])
            ->as('data')
            ->with([
                'data.item',
                'data.item.status'
            ]);
    }

    public function items(){
        return $this->hasMany(Item::class, 'product_id');
    }

    public function rates(){
        return $this->hasMany(ProductRate::class, 'product_id');
    }

    public function media(){
        return $this->hasMany(Media::class, 'product_id');
    }

    public function recalculateRating(){
        $this->rating = $this->rates()->average('value');
        $this->save();
    }

    public function generateSlug(){
        return str_slug($this->name);
    }

    public function getPriceAttribute($value){
        return $value;
    }

    public function getNameAttribute($name){
        if($this->{"name_".app()->getLocale()})
            return $this->{"name_".app()->getLocale()};
        else
            return $this->name_en;
    }

    public function getTitleImageAttribute(){
        $img_name = ($image = $this->media()->first()) && file_exists(public_path($image->name)) ? $image->name : 'storage/none.png';
        return asset($img_name);
    }

    public function toArray()
    {
        $attrs = $this->attributesToArray();
        $rels = $this->relationsToArray();
        $temp = [];
        if(isset($rels['attributes'])){
            foreach($rels['attributes'] as $attribute)
                $temp[$attribute['name']] = $attribute['data']['value'];
            $rels['attributes'] = $temp;
        }
        return array_merge($attrs, $rels);
    }
}
