<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountTransaction extends Model
{
    protected $fillable = [
        'account_id','currency','value','info','date','debit','used_at'
    ];

    protected $casts = [
        'info'=>'object'
    ];

    public function account(){
        return $this->belongsTo(Account::class, 'account_id');
    }
}
