<?php

namespace App\Jobs;

use App\Account;
use App\Events\PaymentCompleteEvent;
use App\Helpers\PaymentSystemInterface;
use App\Item;
use App\Order;
use App\OrderStatus;
use App\Payment;
use App\PaymentStatus;
use App\SiteSetting;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class ReleaseOrderItems implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUEUE = 'order.item_release';
    const DELAY = 120;

    private $order_id;

    /**
     * Create a new job instance.
     * @param $order Order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order_id = $order->id;
    }


    /**
     * @param Payment $payment
     * @param Account|null $account
     * @return PaymentSystemInterface
     */
    private function getPaymentSystemApi(Payment $payment, Account $account = null)
    {
        return $payment->method->createApiInstance($account);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->retrieveModel();
        if(!$order)
            return;

        if($order->payment){
            $account = $this->retrievePaymentAccount($order->payment);
            $api = $this->getPaymentSystemApi($order->payment, $account);
            if(in_array($order->status->name, ['complete']))
                return;
            elseif(($tr = $api->CheckTransactionCompletion("RUR",
                    $order->price,
                    $order->payment->info,
                    $order->payment->created_at->timestamp * 1000
                ))){
                if(in_array($order->status->name, ['created', 'pending'])){
                    $tr->update([
                        'used_at'=>Carbon::now()->timestamp
                    ]);
                    $tr->save();
                    event(new PaymentCompleteEvent($order->payment, $tr));
                }
                return;
            }
        }

        $setting = SiteSetting::where('name', 'item_lock_time')->first();
        if($setting)
            $setting = $setting->value;
        else
            $setting = 5 * 60;
        if(Carbon::now()->diffInSeconds($order->created_at) >= $setting){
            DB::transaction(function() use ($order) {
                $items = [];
                foreach($order->products as $product)
                    foreach($product->data->items as $item)
                        array_push($items, $item->id);
                Item::query()->whereIn('id', $items)->update(['status_id'=>1]);
                $order->status()->associate(OrderStatus::query()->where('name', 'cancelled')->first());
                $order->payment->status()->associate(PaymentStatus::query()->where('name', 'cancelled')->first());
                $order->save();
                $order->payment->save();
            });
            return;
        }

        self::createJob($order);
    }

    public static function getQueueName(Order $order){
        return static::QUEUE . '.' . $order->id;
    }

    public static function createJob(Order $order){
        return dispatch(new static($order))->onQueue(static::getQueueName($order))->delay(static::DELAY);
    }

    /**
     * @return Order|null
     */
    private function retrieveModel(){
        return Order::find($this->order_id);
    }

    /**
     * @param Payment $payment
     * @return Account
     */
    private function retrievePaymentAccount(Payment $payment){
        return $payment->account;
    }
}
