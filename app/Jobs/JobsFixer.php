<?php

namespace App\Jobs;

use App\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class JobsFixer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $accounts = Account::query()->whereHas('status', function($builder){
            $builder->whereIn('name', ['active', 'created']);
        })->get();

        foreach($accounts as $account){
            $jc = DB::table('jobs')
                ->where('queue', AccountKeeper::QUEUE . $account->id)
                ->count();
            if(!$jc){
                AccountKeeper::createJob($account);
            }
        }
        $jc = DB::table('jobs')
            ->where('queue', 'accounts.keeper')
            ->count();
        if($jc==1) static::createJob();
    }

    public static function createJob(){
        dispatch(new static())->onQueue('accounts.keeper')->delay(60);
    }
}
