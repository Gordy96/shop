<?php

namespace App\Jobs;

use App\Account;
use App\AccountStatus;
use App\AccountTransaction;
use App\Events\PaymentCompleteEvent;
use App\Helpers\Payments\Platforms\ManualAccountConfirmationNeededException;
use App\Helpers\PaymentSystemInterface;
use App\Payment;
use App\PaymentMethod;
use App\SiteSetting;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AccountKeeper implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    const QUEUE = 'account.';

    /**
     * @var int
     */
    private $aid;

    /**
     * Create a new job instance.
     * @param Account $aid
     * @return void
     */
    public function __construct($aid)
    {
        $this->aid = $aid->id;
    }

    /**
     * @return Account
     */
    private function getAccount()
    {
        return Account::find($this->aid);
    }

    /**
     * @param Account $account
     * @return PaymentMethod
     */
    private function getPaymentMethod($account)
    {
        return $account->method;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = $this->getAccount();
        if(!$account)
            return;
        $method = $this->getPaymentMethod($account);
        $api = $method->createApiInstance($account);
        $response = false;
        try{
            $response = $api->Auth();
            if($response){
                $account->status()->associate(AccountStatus::where('name', 'active')->first());
                $account->save();
            } else {
                $account->status()->associate(AccountStatus::where('name', 'inactive')->first());
                $account->save();
            }

        } catch(ManualAccountConfirmationNeededException $exception){
            $account->status()->associate(AccountStatus::where('name', 'mail_confirmation')->first());
            $account->save();
            return;
        } catch(\Throwable $exception){
            $account->status()->associate(AccountStatus::where('name', 'error')->first());
            $account->save();
            return;
        }

        if($response)
            $api->getTransactionHistory(function($data) use($account, &$api) {
                Log::info("Account {$account->id} get transaction:", $data);

                if(!$account->transactions()->where('date', $data['date'])->count()){
                    DB::transaction(function() use($data, &$account, &$api){
                        $account->transactions()->save(new AccountTransaction([
                            'currency'=>$data['currency'],
                            'debit'=>$data['debit'],
                            'value'=>$data['value'],
                            'info'=>$data['info'],
                            'date'=>$data['date']
                        ]));

                        $payment = Payment::query()
                            ->with(['order','order.status'])
                            ->where('info', 'like', "%{$data['info']}%")
                            ->latest()
                            ->first();
                        if($payment && $payment->order->status->name !== "complete"){
                            if(($tr = $api->CheckTransactionCompletion("RUR",
                                $payment->order->price,
                                $payment->info,
                                $payment->created_at->timestamp * 1000
                            ))){
                                $tr->update([
                                    'used_at'=>Carbon::now()->timestamp
                                ]);
                                $tr->save();
                                event(new PaymentCompleteEvent($payment, $tr));
                            }
                        }
                    });
                }
                return true;
            });

        $new_job = static::createJob($account);
        if($new_job){
            if($response === false){
                $setting = SiteSetting::query()->where('name', 'account_keeper_delay_captcha')->first();
                $new_job->delay($setting ? $setting->value : 2*60);
            }
            elseif($response === true){
                $setting = SiteSetting::query()->where('name', 'account_keeper_delay_authorised')->first();
                $new_job->delay($setting ? $setting->value : 5*60);
            }
        }
    }

    public static function getQueueName(Account $account){
        return static::QUEUE . $account->id;
    }

    public static function createJob(Account $account){
        if(DB::table('jobs')->where('queue', static::getQueueName($account))->count() <= 1)
            return dispatch(new static($account))->onQueue(static::getQueueName($account));
    }
}
