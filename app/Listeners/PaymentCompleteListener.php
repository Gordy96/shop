<?php

namespace App\Listeners;

use App\AccountTransaction;
use App\Events\PaymentCompleteEvent;
use App\Exceptions\NotEnoughItemsException;
use App\Item;
use App\ItemStatus;
use App\OrderStatus;
use App\PaymentStatus;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaymentCompleteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentCompleteEvent $event
     * @return void
     */
    public function handle($event)
    {
        if($event->payment()->order->status->name === "cancelled"){
            $order = $event->payment()->order;
            try{
                DB::transaction(function() use($order, $event) {
                    $ids = [];
                    foreach($order->products as $product){
                        $items = Item::query()
                            ->where('product_id', $product->id)
                            ->where('status_id', 1)
                            ->limit($product->data->items->count())
                            ->get();
                        if($items->count() !== $product->data->items->count())
                            throw new NotEnoughItemsException();
                        foreach($items as $item)
                            $ids[$item->id] = ["product_id"=>$product->id];
                    }
                    Item::query()->whereIn('id', array_keys($ids))->update(['status_id'=>3]);
                    $order->items()->sync($ids);
                    $order->status()->associate(OrderStatus::query()->where('name', 'complete')->first());
                    $order->save();
                    $event->payment()->status()->associate(PaymentStatus::where('name','complete')->first());
                    $event->payment()->save();
                    if(!$event->transaction()->used_at){
                        $event->transaction()->update([
                            'used_at'=>Carbon::now()->timestamp
                        ]);
                        $event->transaction()->save();
                    }
                });
            } catch(NotEnoughItemsException $e){
                Log::error('Could not find enough available items to finish this order', ['order'=>$order]);
                $order->status()->associate(OrderStatus::query()->where('name', 'error')->first());
                $order->save();
            }
        } elseif (in_array($event->payment()->order->status->name, ["created", "pending"])) {
            DB::transaction(function() use ($event) {
                $event->payment()->status()->associate(PaymentStatus::where('name','complete')->first());
                $event->payment()->order->status()->associate(OrderStatus::where('name','complete')->first());
                $event->payment()->order->save();
                $event->payment()->save();
                $ids = [];
                foreach($event->payment()->order->items as $item)
                    array_push($ids, $item->id);
                Item::whereIn('id', $ids)->update([
                    'status_id'=>ItemStatus::query()->where('name', 'sold')->first()->id
                ]);
                if(!$event->transaction()->used_at){
                    $event->transaction()->update([
                        'used_at'=>Carbon::now()->timestamp
                    ]);
                    $event->transaction()->save();
                }
            });
        }
    }
}
