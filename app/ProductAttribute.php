<?php

namespace App;

use App\Helpers\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function products(){
        return $this->belongsToMany(Product::class, 'product_product_attribute', 'attribute_id', 'product_id')
            ->withPivot(['value'])
            ->as('data');
    }
}
