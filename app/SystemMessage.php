<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemMessage extends Model
{
    protected $fillable = [
        'type',
        'text'
    ];
}
