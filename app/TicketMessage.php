<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $fillable = [
        'ticket_id',
        'user_id',
        'text',
        'read_at'
    ];

    public $dates = [
        'created_at',
        'updated_at',
        'read_at'
    ];

    public function read(){
        $this->read_at = Carbon::now();
        $this->save();
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function ticket(){
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }
}
