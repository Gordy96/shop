<?php

namespace App\Observers;

use App\Product;

class ProductObserver
{

    public function creating(Product $product)
    {
        $product->slug = $product->generateSlug();
    }
    /**
     * Handle to the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        //
    }

    public function saving(Product $product)
    {
        $product->slug = $product->generateSlug();
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        //
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }
}
