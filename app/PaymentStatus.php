<?php

namespace App;

use App\Helpers\Eloquent\Model;

class PaymentStatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function payments(){
        return $this->hasMany(Payment::class, 'method_id');
    }
}
