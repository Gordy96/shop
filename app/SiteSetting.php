<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{

    protected $fillable = [
        'name',
        'value',
        'type',
        'parameters'
    ];

    protected $casts = [
        'parameters'=>'array'
    ];

    public $timestamps = false;

    protected function getCastType($key)
    {
        if ($key == 'value' && !empty($this->type)) {
            return $this->type;
        } else {
            return parent::getCastType($key);
        }
    }

    public function getValueAttribute(){
        return $this->castAttribute('value', $this->attributes['value']);
    }
}
