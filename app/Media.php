<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'name',
        'product_id',
        'type_id'
    ];

    public $timestamps = false;

    public function product()
    {
        if($this->product_id){
            return $this->belongsTo(Product::class, 'product_id');
        }
        return null;
    }

    public function type()
    {
        return $this->belongsTo(MediaType::class, 'type_id');
    }
}
